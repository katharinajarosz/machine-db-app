/**
 * 
 */
package com.hw.importservice.filereader;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;

import io.netty.buffer.ByteBuf;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.hw.parser.beans.RHeaderData;
import com.hw.parser.beans.RMachineDataSet;
import com.hw.parser.beans.headerdata.RCalibrationData;
import com.hw.parser.beans.headerdata.RFileHeader;
import com.hw.parser.beans.headerdata.RSystemConfiguration;
import com.hw.parser.beans.machinedata.RAnalogIn;
import com.hw.parser.beans.machinedata.RBalancingData;
import com.hw.parser.beans.machinedata.RClosedLoop;
import com.hw.parser.beans.machinedata.RDigitalInput;
import com.hw.parser.beans.machinedata.RDigitalOutput;
import com.hw.parser.beans.machinedata.RErrorData;
import com.hw.parser.beans.machinedata.RErrorElement;
import com.hw.parser.beans.machinedata.RIOData;
import com.hw.parser.beans.machinedata.RNumericInput;
import com.hw.parser.beans.machinedata.RNumericOutput;
import com.hw.parser.beans.machinedata.RProcessControlData;
import com.hw.parser.beans.machinedata.RProtectiveSystemData;
import com.hw.parser.beans.machinedata.RPumpData;
import com.hw.parser.beans.machinedata.RSondData;
import com.hw.parser.beans.machinedata.RSystemDiagnosis;

/**
 * Read HWBIN binary files that contain the machine data.
 * 
 * @author kaja
 * @version 1.0
 *
 */

public class FileReader {

	/**
	 * Returns an instance of {@link RHeaderData} that represent the values of
	 * this {@code ByteBuffer}.
	 * <p>
	 * This method read {@link RHeaderData} data from this current
	 * {@code ByteBuffer} using the methods: <blockquote>
	 * 
	 * <pre>
	 * {@link #readFileHeader(ByteBuffer)}
	 * {@link #readSystemConfiguration(ByteBuffer)}
	 * {@link #readCalibrationData(ByteBuffer)}
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * 
	 * @return An instance of {@link RHeaderData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	public RHeaderData readHeaderData(ByteBuf bb) throws IOException {

		return RHeaderData
				.builder()
				.fileHeader(this.readFileHeader(bb))
				.systemConfiguration(this.readSystemConfiguration(bb))
				.calibrationData(this.readCalibrationData(bb))
				.build();

	}

	/**
	 * Returns an instance of {@link RMachineDataSet} that represent the values
	 * of this {@code ByteBuffer}.
	 * <p>
	 * This method read one {@link RMachineDataSet} from this current
	 * {@code ByteBuffer} using the methods: <blockquote>
	 * 
	 * <pre>
	 * {@link #readNumericInputs(ByteBuffer)}
	 * {@link #readDigitalInputs(ByteBuffer)}
	 * {@link #readClosedLoops(ByteBuffer)}
	 * {@link #readNumericOutputs(ByteBuffer)}
	 * {@link #readDigitalOutputs(ByteBuffer)}
	 * {@link #readIoData(ByteBuffer)}
	 * {@link #readProcessControlData(ByteBuffer)}
	 * {@link #readProtectiveSystemData(ByteBuffer)}
	 * {@link #readSystemDiagnosis(ByteBuffer)}
	 * {@link #readErrorData(ByteBuffer)}
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RMachineDataSet}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	public RMachineDataSet readMachineDataSet(ByteBuf bb) throws IOException {

		List<RNumericInput> numericInputs = readNumericInputs(bb);
		List<RDigitalInput> digitalInputs = readDigitalInputs(bb);
		List<RClosedLoop> closedLoops = readClosedLoops(bb);
		List<RNumericOutput> numericOutputs = readNumericOutputs(bb);
		List<RDigitalOutput> digitalOutputs = readDigitalOutputs(bb);
		RIOData ioData = readIoData(bb);
		RProcessControlData processControlData = readProcessControlData(bb);
		RProtectiveSystemData protectiveSystemData = readProtectiveSystemData(bb);
		RSystemDiagnosis systemDiagnosis = readSystemDiagnosis(bb);
		RErrorData errorData = readErrorData(bb);

		return RMachineDataSet
				.builder()
				.numericInput(numericInputs)
				.digitalInput(digitalInputs)
				.closedLoop(closedLoops)
				.numericOutput(numericOutputs)
				.digitalOutput(digitalOutputs)
				.ioData(ioData)
				.processControlData(processControlData)
				.protectiveSystemData(protectiveSystemData)
				.systemDiagnosis(systemDiagnosis)
				.errorData(errorData)
				.build();

	}

	/**
	 * Returns an instance of {@link RFileHeader} that represent the values of
	 * this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RFileHeader}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RFileHeader readFileHeader(ByteBuf bb) throws IOException {

		return RFileHeader
				.builder()
				.fileIdentifier(bb.readCharSequence(2, Charset.forName("utf-8")).toString())
				.sectionSize(bb.readInt())
				.offset(bb.readInt())
				.fileFormatVersion(new BigDecimal(bb.readInt()).movePointLeft(2))
				.build();

	}

	/**
	 * Returns an instance of {@link RSystemConfiguration} that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RSystemConfiguration}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RSystemConfiguration readSystemConfiguration(ByteBuf bb) throws IOException {

		bb.readerIndex(16);

		return RSystemConfiguration
				.builder()
				.sectionSize(bb.readInt())
				.machineNumber(bb.readCharSequence(4, Charset.forName("utf-8")).toString())
				.hostVersion(bb.readCharSequence(24, Charset.forName("utf-8")).toString())
				.controlVersion(bb.readCharSequence(24, Charset.forName("utf-8")).toString())
				.protectVersion(bb.readCharSequence(24, Charset.forName("utf-8")).toString())
				.parameterFile(bb.readCharSequence(128, Charset.forName("utf-8")).toString())
				.clinicName(bb.readCharSequence(128, Charset.forName("utf-8")).toString())
				.build();

	}

	/**
	 * Returns an instance of {@link RCalibrationData} that represent the values
	 * of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RCalibrationData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RCalibrationData readCalibrationData(ByteBuf bb) throws IOException {

		List<Double> calibration = new LinkedList<>();
		
		bb.readerIndex(512);
		int sectionSize = bb.readInt();
		int numberOfEntries = bb.readInt();
		
		for(int i = 0; i < numberOfEntries; i++) {
			double d = bb.readDouble();
			d = Double.isNaN(d)? 0 : d;
			calibration.add(d);
		}
		
		RCalibrationData calibrationData = RCalibrationData
				.builder()
				.sectionSize(sectionSize)
				.numberOfEntries(numberOfEntries)
				.calibration(calibration).build();

		return calibrationData;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RNumericInput}'s that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RNumericInput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RNumericInput> readNumericInputs(ByteBuf bb) throws IOException {

		List<RNumericInput> numericInputs = new ArrayList<>();
		int size = bb.readInt();
		for (int i = 0; i < size; i++) {
			numericInputs.add(RNumericInput
					.builder()
					
					.numericInput(bb.readInt())
					.r(bb.readFloat())
					.f(bb.readFloat())
					.cd(bb.readFloat())
					.build());
		}

		return numericInputs;

	}

	/**
	 * Returns an {@code ArrayList} of {@link RDigitalInput}'s that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RDigitalInput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RDigitalInput> readDigitalInputs(ByteBuf bb) throws IOException {

		List<RDigitalInput> digitalInputs = new ArrayList<>();
		int size = bb.readInt();
		for (int i = 0; i < size; i++) {

			digitalInputs.add(RDigitalInput
					.builder()
					
					.digitalInput(bb.readInt())
					.cd(bb.readBoolean())
					.r(bb.readBoolean())
					.build());
		}

		return digitalInputs;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RClosedLoop}'s that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RClosedLoop}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RClosedLoop> readClosedLoops(ByteBuf bb) throws IOException {

		List<RClosedLoop> closedLoops = new ArrayList<>();
		int size = bb.readInt();
		for (int i = 0; i < size; i++) {
			closedLoops.add(RClosedLoop
					.builder()
					
					.closedLoop(bb.readInt())
					.hmi(bb.readFloat())
					.proc(bb.readFloat())
					.dem(bb.readFloat())
					.y(bb.readFloat())
					.inp(bb.readFloat())
					.e(bb.readFloat())
					.build());
		}

		return closedLoops;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RNumericOutput}'s that represent
	 * the values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RNumericOutput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RNumericOutput> readNumericOutputs(ByteBuf bb) throws IOException {

		List<RNumericOutput> numericOutputs = new ArrayList<>();
		int size = bb.readInt();
		for (int i = 0; i < size; i++) {

			numericOutputs.add(RNumericOutput
					.builder()
					
					.numericOutput(bb.readInt())
					.dem(bb.readFloat())
					.cd(bb.readFloat())
					.build());
		}

		return numericOutputs;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RDigitalOutput}'s that represent
	 * the values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RDigitalOutput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RDigitalOutput> readDigitalOutputs(ByteBuf bb) throws IOException {

		List<RDigitalOutput> digitalOutputs = new ArrayList<>();
		int size = bb.readInt();
		for (int i = 0; i < size; i++) {
			digitalOutputs.add(RDigitalOutput
					.builder()
					
					.digitalOutputs(bb.readInt())
					.hmi(bb.readBoolean())
					.dem(bb.readBoolean())
					.build());
		}

		return digitalOutputs;
	}

	/**
	 * Returns an instance of {@link RIOData} that represent the values of this
	 * {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RIOData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RIOData readIoData(ByteBuf bb) throws IOException {

		return RIOData
				.builder()
				.timestamp1(bb.readLong())
				.timestamp2(bb.readLong())
				.supplyMode(bb.readByte())
				.wasteMode(bb.readByte())
				.gpMode(bb.readByte())
				.heaterMode(bb.readByte())
				.build();

	}

	/**
	 * Returns an instance of {@link RProcessControlData} that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RProcessControlData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RProcessControlData readProcessControlData(ByteBuf bb) throws IOException {

		return RProcessControlData
				.builder()
				.systemState(bb.readByte())
				.idleState(bb.readByte())
				.startupState(bb.readByte())
				.serviceState(bb.readByte())
				.preparationState(bb.readByte())
				.treatmentState(bb.readByte())
				.postprocessState(bb.readByte())
				.desinfectionState(bb.readByte())
				.activeSubsequence(bb.readByte())
				.subsequenceState(bb.readByte())
				.build();
	}

	/**
	 * Returns an instance of {@link RProtectiveSystemData} that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RProtectiveSystemData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RProtectiveSystemData readProtectiveSystemData(ByteBuf bb) throws IOException {

		List<RSondData> sondData = new LinkedList<>();
		for (int i = 0; i < 4; i++) {
			sondData.add(RSondData
					.builder()
					.upH(bb.readShort())
					.uTemp(bb.readShort())
					.temperature(bb.readFloat())
					.ph(bb.readFloat())
					.build());

		}
		//log.info("/n sond data: "+sondData);

		List<RPumpData> pumpData = new LinkedList<>();
		for (int i = 0; i < 2; i++) {
			pumpData.add(RPumpData
					.builder()
					.host(bb.readFloat())
					.control(bb.readFloat())
					.meas(bb.readFloat())
					.build());

		}

		List<RAnalogIn> analogIn = new LinkedList<>();
		for (int i = 0; i < 10; i++) {
			analogIn.add(RAnalogIn
					.builder()
					.f(bb.readFloat())
					.cd(bb.readFloat())
					.build());

		}

		RBalancingData balancingData = RBalancingData
				.builder()
				.ufHost(bb.readFloat())
				.ufControl(bb.readFloat())
				.ufWeight(bb.readFloat())
				.acidFlowConrol(bb.readFloat())
				.acidWeight(bb.readFloat())
				.acidDensity(bb.readFloat())
				.baseFlowControl(bb.readFloat())
				.baseWeight(bb.readFloat())
				.baseDensity(bb.readFloat())
				.scale1row(bb.readFloat())
				.scale2row(bb.readFloat())
				.scaleDeviation(bb.readFloat())
				.scaleSumCurrent(bb.readFloat())
				.build();

		return RProtectiveSystemData
				.builder()
				.unknownMessageCount(bb.readInt())
				.crcErrorCount(bb.readInt())
				.messageCounterErrorCount(bb.readInt())
				.sendMsgCounter(bb.readByte())
				.protectState(bb.readByte())
				.protectSubstate(bb.readByte())
				.controlStatusByte(bb.readByte())
				.protectStatusByte(bb.readByte())
				.sondData(sondData)
				.pumpData(pumpData)
				.analogIn(analogIn)
				.lastReceivedMessageID(bb.readByte())
				.lastSendMessageID(bb.readByte())
				.lastSendMainState(bb.readByte())
				.lastSendSubState(bb.readByte())
				.rs232CommunicationLoopExecutionTime(bb.readInt())
				.balancingData(balancingData)
				.build();

	}

	/**
	 * Returns an instance of {@link RSystemDiagnosis} that represent the values
	 * of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RSystemDiagnosis}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RSystemDiagnosis readSystemDiagnosis(ByteBuf bb) throws IOException {

		return RSystemDiagnosis
				.builder()
				.processorLoad(bb.readBytes(4).readFloat())
				.memoryUsage(bb.readFloat())
				.systemLoopProcessingTime(bb.readFloat())
				.loopLateCounter(bb.readFloat())
				.hostTotalProcessorLoad(bb.readFloat())
				.hostLK2001ProcessorLoad(bb.readFloat())
				.hostLK2001PrivateBytes(bb.readFloat())
				.hostReconnectCounter(bb.readFloat())
				.hostConnectionStageChangeCounter(bb.readFloat())
				.durationLastSavingProcess(bb.readFloat())
				.open(bb.readFloat())
				.setPointerToEnd(bb.readFloat())
				.sdwrite(bb.readFloat())
				.sdflush(bb.readFloat())
				.sdclose(bb.readFloat())
				.elementInQ(bb.readFloat())
				.build();

	}

	/**
	 * Returns an instance of {@link RErrorData} that represent the values of
	 * this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RErrorData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RErrorData readErrorData(ByteBuf bb) throws IOException {

		List<RErrorElement> errorElements = new ArrayList<>();
		int size = bb.readInt();
		for (int i = 0; i < size; i++) {
			errorElements.add(RErrorElement
					.builder()
					.errorId(bb.readInt())
					.errorLevel(bb.readInt())
					.build());

		}

		float phReservoirRangeCheck = bb.readFloat();

		return RErrorData
				.builder()
				.errorElements(errorElements)
				.phReservoirRangeCheck(phReservoirRangeCheck)
				.build();

	}
}