package com.hw.importservice.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

// TODO: Auto-generated Javadoc
/**
 * Converts a {@link Long} of LabView time object into {@link LocalDateTime}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
public class LocalDateTimeConverter {

	public LocalDateTime convert(Long source, Long nano) {

		long labViewUTC = -2082844800;
		long target = labViewUTC + source;
		Instant instant = Instant.ofEpochSecond(source, nano);
		int nanoSeconds = instant.getNano();
		LocalDateTime localTimestamp = LocalDateTime.ofEpochSecond(target, nanoSeconds, ZoneOffset.ofHours(1));

		return localTimestamp;
	}

	public Instant convertInstant(Long source, Long nano) {

		long labViewUTC = -2082844800;
		long target = labViewUTC + source;
		Instant timestamp = Instant.ofEpochSecond(source, nano);

		return timestamp;
	}

}