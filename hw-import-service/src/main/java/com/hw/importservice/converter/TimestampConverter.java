package com.hw.importservice.converter;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;

// TODO: Auto-generated Javadoc
/**
 * Converts a {@link Long} of LabView time object into {@link Timestamp}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
public class TimestampConverter  {
		
		public Timestamp convert(Long seconds, Long nano) {
			
			Instant instant = Instant.ofEpochSecond(seconds, nano);
			int nanos = instant.getNano();
			long labViewUTC = -2082844800;
			long unixUTC = labViewUTC + seconds;
			
			Instant i = Instant.ofEpochSecond(unixUTC);
			
			Timestamp utc = Timestamp.from(i);
			utc.setNanos(nanos);
			return utc;
		}

	}