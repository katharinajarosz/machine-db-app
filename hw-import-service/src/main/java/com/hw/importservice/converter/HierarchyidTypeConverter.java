package com.hw.importservice.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HierarchyidTypeConverter implements AttributeConverter<String, Byte[]> {
	
	@Autowired 
	private EntityManager em;
	
	public String convertToString(int...is) {
		String node = "/";
		for (int i : is) {
			node= node + i + node;
		}
		return node;
	}

	@Override
	public Byte[] convertToDatabaseColumn(String attribute) {
		
		Query query = em.createNativeQuery(
				"SELECT system_state_node " +
				"FROM system_state " +  
				"WHERE system_state_node.ToString() =?;"
				, Byte[].class);
		query.setParameter(1, attribute);			
								
		Byte[] node = null;
		try {
 			node = (Byte[]) query.getSingleResult();
 		} catch (NoResultException e) {

		}	
		return node;
	}

	@Override
	public String convertToEntityAttribute(Byte[] dbData) {
		
		Query query = em.createNativeQuery(
				"SELECT system_state_node.ToString() " +
				"FROM system_state " +  
				"WHERE system_state_node =?;"
				, String.class);
		query.setParameter(1, dbData);			
								
		String node = null;
		try {
 			node = (String) query.getSingleResult();
 		} catch (NoResultException e) {

		}	
		return node;
	}

}
