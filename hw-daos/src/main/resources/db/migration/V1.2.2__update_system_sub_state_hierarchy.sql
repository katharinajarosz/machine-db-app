IF OBJECT_ID('AddSubState','P') IS NOT NULL
   DROP PROCEDURE AddSubState
GO

CREATE PROC AddSubState(@parentStateName varchar(50), @stateId int OUTPUT, @stateName varchar(50))   
AS   
BEGIN  
   DECLARE @parentNode hierarchyid, @lastChildNode hierarchyid
   SELECT @parentNode = system_state_node
   FROM machine.system_state
   WHERE system_state_name = @parentStateName
   AND system_state_level = 1
   
   SET TRANSACTION ISOLATION LEVEL SERIALIZABLE  
   BEGIN TRANSACTION   
		
      SELECT @lastChildNode = max(system_state_node)
		FROM machine.system_state
		WHERE system_state_node.GetAncestor(1) = @parentNode
	  INSERT machine.system_state (system_state_node, system_state_value, system_state_name)  
      VALUES(@parentNode.GetDescendant(@lastChildNode, NULL), @stateId, @stateName)
	  SET @stateId = @stateId+1;  
   COMMIT  
END ;  
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Startup';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'LoadConfigInit';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'LoadConfigWaitForData';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'LoadConfigFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'LoadConfigPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'CheckServiceSwitchState';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'CommProtProtectCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'CommProtRtCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'CommProtFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'CommProtPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PowerRelayCheckPumpDoors';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PowerRelayTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PowerRelayFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PowerRelayPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SupplyVoltCheck1';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SupplyVoltWaitForProtectPower';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SupplyVoltProtectPowerOn';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SupplyVoltCheck2';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SupplyVoltFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SupplyVoltPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PHTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PHFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PHPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HWKeysTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HWKeysFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HWKeysPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BuzzerRTOn';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BuzzerRTOff';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BuzzerProtectOn';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BuzzerFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BuzzerPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeOff0FB';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeOff100FB';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeOff0';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeProtectSwitchRelaisOn';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeProtectRelaisOn';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeOn0FB1';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeOn100FB';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeOn0FB2';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafeFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HeatSafePassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostCommRTCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostCommHostCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostCommFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostCommPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'InitCheckForWarmResetData';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'CheckForWarmResetData';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ScalesTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ScalesFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ScalesPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BloodPresZeroCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BloodPresFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BloodPresPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpDoorCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpStopedTest1';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpStopedTest1Confirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpsRunningTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpsRunningTestConfirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpStopedTest2';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpStopedTest2Confirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PeristPumpPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ArtBloodZeroCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ArtBloodZeroCheckConfirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ArtBloodRangeCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ArtBloodFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ArtBloodPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BLDZeroCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BLDZeroCheckConfirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BLDRangeCheck';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BLDFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'BLDPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampOffTest1';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampOffTest1Confirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampOnTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampOnTestConfirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampOffTest2';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampOffTest2Confirm';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'VenClampPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PPTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PPFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PPPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'GPTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'GPFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'GPPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'AirDetEmptyTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'AirDetFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'AirDetPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostUSBTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostUSBFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'HostUSBPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SystemExecutionFinished';
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Idle';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputs';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SystemExecutionFinished';
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Service';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'CheckServicePassword';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputs';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_Filling';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_Empty';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_CitricHotDisinfection';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_StandardDisinfection';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_ROFilling';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_ROEmpty';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_SelfTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SystemExecutionFinished';
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Preparation';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputs';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1SondsRinse';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1SondsTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1SondsFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1SondsPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1RefillHydraulic';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ConcSettle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ConcTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ConcFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ConcPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtPPIntNPCreatePP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtPPIntNPCreateNP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtPPIntNPSettle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtPPIntNPTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtPPIntNPFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtPPIntNPPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass1Settle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass1Test';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass1Failed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass1Passed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtNPIntPPCreateNP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtNPIntPPCreatePP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtNPIntPPSettle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtNPIntPPTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtNPIntPPFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1ExtNPIntPPPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass2Settle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass2Test';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass2Failed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P1Bypass2Passed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsT1P1Passed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_RemoveDisinfectant';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsHydraulicFilled';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_FillSystem';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsSystemFilled';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2BLDRinse';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2BLDTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2BLDFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2BLDPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterNPCreateNP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterNPSettle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterNPTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterNPFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterNPPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterPPCreatePP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterPPSettle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterPPTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterPPFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2FilterPPPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2OccCreatePP';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2OccSettle';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2OccTest';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2OccFailed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'T1P2OccPassed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsT1P2Passed';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_IntakeAlbumin';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsSystemReady';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SystemExecutionFinished';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_RefillHydraulic';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_RefillSystem';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_ROFillingBeforeT1Test';
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Treatment';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'WaitForTreatment';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_WaitForBlood';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_FillTubesetWithBlood';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ReadyForHepaWashStart';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_StartBalancing';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Bypass';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'PatientTreatment';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_RestartBalancing';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_ConnectHepaWash';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'FluidReplacement';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Circulation';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'DisconnectPatient';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'WarmResetExecuted';
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Postprocess';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputs';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_RemoveDialyzerAndFilter';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'SystemExecutionFinished';
GO

DECLARE @stateId int = 0, @parentStateName varchar(50);
SET @parentStateName = 'Desinfection';

EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputs';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_CitricHotDisinfection';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_RepeatedCitricHotDisinfection';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_StandardDisinfection';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_SporotalPart1Soda';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsSporotalPart1';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_SporotalPart2Sporotal';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'ProcessUserInputsSporotalPart2';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_SporotalOptionalRinse';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_SporotalWasteSample';
EXEC AddSubState @parentStateName,@stateId OUTPUT,'Seq_SporotalPart3CitricHotDisinfection';
GO

IF OBJECT_ID('AddSubState','P') IS NOT NULL
   DROP PROCEDURE AddSubState
GO
