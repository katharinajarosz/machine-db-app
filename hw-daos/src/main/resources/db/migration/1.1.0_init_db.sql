
USE [Machine_Data_DB]
GO

/****** Object:  UserDefinedTableType [dbo].[AnalogInTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[AnalogInTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[converted_data] [float] NULL,
	[filtered_data] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[BalancingDataTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[BalancingDataTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[acid_flow_conrol] [float] NULL,
	[acid_weight] [float] NULL,
	[base_flow_control] [float] NULL,
	[base_weight] [float] NULL,
	[scale1row] [float] NULL,
	[scale2row] [float] NULL,
	[scale_deviation] [float] NULL,
	[scale_sum_current] [float] NULL,
	[uf_control] [float] NULL,
	[uf_host] [float] NULL,
	[uf_weight] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[ClosedLoopTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[ClosedLoopTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [float] NULL,
	[e] [float] NULL,
	[hmi] [float] NULL,
	[inp] [float] NULL,
	[procx] [float] NULL,
	[y] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[closed_loop_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[DigitalInputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[DigitalInputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [bit] NULL,
	[r] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_input_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[DigitalOutputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[DigitalOutputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [bit] NULL,
	[hmi] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_output_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[NumericInputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[NumericInputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[f] [float] NULL,
	[r] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_input_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[NumericOutputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[NumericOutputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[dem] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_output_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[PumpDataTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[PumpDataTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[control] [float] NULL,
	[host] [float] NULL,
	[measure] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SondDataTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[SondDataTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[calculated_ph] [float] NULL,
	[calculated_temperature] [float] NULL,
	[measured_temperature] [int] NULL,
	[measured_ph] [int] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SystemDiagnosisTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [dbo].[SystemDiagnosisTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[host_lk_bytes] [float] NULL,
	[host_lk_processor_load] [float] NULL,
	[host_reconnect_counter] [float] NULL,
	[host_processor_load] [float] NULL,
	[loop_late_counter] [float] NULL,
	[memory_load] [float] NULL,
	[processor_load] [float] NULL,
	[system_loop_processing_time] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO

/****** Object:  Table [dbo].[active_error]    Script Date: 10.10.2019 17:53:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[active_error](
	[active_error_id] [bigint] IDENTITY(1,1) NOT NULL,
	[error_id] [int] NULL,
	[error_level_id] [int] NULL,
	[machine_run_id] [bigint] NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
PRIMARY KEY CLUSTERED 
(
	[active_error_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[analog_in]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[analog_in](
	[analog_in_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[converted_data] [float] NULL,
	[filtered_data] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[analog_in_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[balancing_data]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[balancing_data](
	[balancing_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[acid_flow_conrol] [float] NULL,
	[acid_weight] [float] NULL,
	[base_flow_control] [float] NULL,
	[base_weight] [float] NULL,
	[scale1row] [float] NULL,
	[scale2row] [float] NULL,
	[scale_deviation] [float] NULL,
	[scale_sum_current] [float] NULL,
	[uf_control] [float] NULL,
	[uf_host] [float] NULL,
	[uf_weight] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[balancing_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[calibration_data]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[calibration_data](
	[calibration_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [varchar](255) NULL,
	[value] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[calibration_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[closed_loop]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[closed_loop](
	[closed_loop_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [float] NULL,
	[e] [float] NULL,
	[hmi] [float] NULL,
	[inp] [float] NULL,
	[procx] [float] NULL,
	[y] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[closed_loop_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[closed_loop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[closed_loop_type]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[closed_loop_type](
	[closed_loop_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[closed_loop_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[digital_input]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digital_input](
	[digital_input_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [bit] NULL,
	[r] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_input_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[digital_input_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[digital_input_type]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digital_input_type](
	[digital_input_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[digital_input_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[digital_output]    Script Date: 04.11.2019 15:56:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digital_output](
	[digital_output_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [bit] NULL,
	[hmi] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_output_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[digital_output_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[digital_output_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digital_output_type](
	[digital_output_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[digital_output_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------error--------------------------------------------
/****** Object:  Table [dbo].[error]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error](
	[error_id] [int] NOT NULL,
	[error_symbol] [varchar](255) NULL,
	[error_text_user] [varchar](600) NULL,
	[solution_for_user] [varchar](600) NULL,
	[error_text_service] [varchar](600) NULL,
	[solution_for_service] [varchar](600) NULL,
	[reviewed_von] [varchar](255) NULL,
	[reviewed_am] [varchar](255) NULL,
	[note] [varchar](600) NULL,
	[state] [varchar](255) NULL,
	[severity] [varchar](255) NULL,
	[abortion] [varchar](255) NULL,
	[deprecated] [varchar](255) NULL,										
PRIMARY KEY CLUSTERED 
(
	[error_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------error-----------------------------------------------
/****** Object:  Table [dbo].[error_level]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_level](
	[error_level_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
	[severity] [varchar](255) NULL,
	[description] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[error_level_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[gp_mode_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gp_mode_type](
	[gp_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[gp_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[heater_mode_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[heater_mode_type](
	[heater_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[heater_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------
/****** Object:  Table [dbo].[iodata]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[iodata](
	[io_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ende] [datetime2](7) NULL,
	[start] [datetime2](7) NULL,
	[gp_mode_type_id] [int] NULL,
	[heater_mode_type_id] [int] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[pp_supply_mode_type_id] [int] NULL,
	[pp_waste_mode_type_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[io_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[machine_run]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[machine_run](
	[machine_run_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ende] [datetime2](7) NULL,
	[file_name] [varchar](255) NULL,
	[start] [datetime2](7) NULL,
	[clinic_id] [bigint] NULL,
	[machine_info_id] [bigint] NULL,
	[sond_data_config_id] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[machine_run_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[machine_run_calibraton_data]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[machine_run_calibraton_data](
	[machine_run_id] [bigint] NOT NULL,
	[calibration_data_id] [bigint] NOT NULL
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[numeric_input]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numeric_input](
	[numeric_input_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[f] [float] NULL,
	[r] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_input_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[numeric_input_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[numeric_input_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numeric_input_type](
	[numeric_input_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[numeric_input_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[numeric_output]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numeric_output](
	[numeric_output_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[dem] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_output_type_id] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[numeric_output_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[numeric_output_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numeric_output_type](
	[numeric_output_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[numeric_output_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[pp_supply_mode_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pp_supply_mode_type](
	[pp_supply_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[pp_supply_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[pp_waste_mode_type]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pp_waste_mode_type](
	[pp_waste_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[pp_waste_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[protect_state]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[protect_state](
	[protect_state_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ende] [datetime2](7) NULL,
	[protect_state] [int] NULL,
	[protect_substate] [int] NULL,
	[start] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[protect_state_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------???
/****** Object:  Table [dbo].[psconfiguration]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[psconfiguration](
	[acid_density] [float] NULL,
	[base_density] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[machine_run_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[psmissed_message]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[psmissed_message](
	[ps_missed_message_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ende] [datetime2](7) NULL,
	[message_counter_error_count] [int] NULL,
	[send_message_counter] [int] NULL,
	[start] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ps_missed_message_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------
/****** Object:  Table [dbo].[psstatus]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[psstatus](
	[ps_status_id] [bigint] IDENTITY(1,1) NOT NULL,
	[control_status_byte] [smallint] NULL,
	[ende] [datetime2](7) NULL,
	[protect_status_byte] [smallint] NULL,
	[start] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ps_status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[pump_data]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pump_data](
	[pump_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[control] [float] NULL,
	[host] [float] NULL,
	[measure] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[pump_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[sond_data]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sond_data](
	[sond_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[calculated_ph] [float] NULL,
	[calculated_temperature] [float] NULL,
	[measured_temperature] [int] NULL,
	[measured_ph] [int] NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[sond_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
--------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[sond_data_configuration]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sond_data_configuration](
	[sond_data_configuration_id] [bigint] IDENTITY(1,1) NOT NULL,
	[serial_number] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[sond_data_configuration_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[system_diagnosis]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[system_diagnosis](
	[system_diagnosis_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[host_lk_bytes] [float] NULL,
	[host_lk_processor_load] [float] NULL,
	[host_reconnect_counter] [float] NULL,
	[host_processor_load] [float] NULL,
	[loop_late_counter] [float] NULL,
	[memory_load] [float] NULL,
	[processor_load] [float] NULL,
	[system_loop_processing_time] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[system_diagnosis_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
-----------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[running_system_state]    Script Date: 04.11.2019 15:56:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[running_system_state](
	[running_system_state_id] [bigint] IDENTITY(1,1) NOT NULL,
	[ende] [datetime2](7) NULL,
	[start] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
	[subsequence_node] [hierarchyid] NULL,
	[system_state_node] [hierarchyid] NULL,
PRIMARY KEY CLUSTERED 
(
	[running_system_state_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[system_state]    Script Date: 10.03.2020 16:12:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[system_state](
	[system_state_node] [hierarchyid] NOT NULL,
	[system_state_level]  AS ([System_State_Node].[GetLevel]()),
	[node_string]  AS ([System_State_Node].[ToString]()),
	[system_state_name] [varchar](255) NULL,
	[system_state_value] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[system_state_node] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
---------------------------------------------------------------------------------------------------
/****** Object:  Table [dbo].[system_state_subsequence]    Script Date: 10.03.2020 16:12:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[system_state_subsequence](
	[subsequence_node] [hierarchyid] NOT NULL,
	[subsequence_id] [int] NULL,
	[subsequence_level]  AS ([Subsequence_Node].[GetLevel]()),
	[subsequence_node_string]  AS ([Subsequence_Node].[ToString]()),
	[subsequence_name] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[subsequence_node] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
------------------------------------------------------------------------------------------------------------------------

ALTER TABLE [dbo].[active_error]  WITH CHECK ADD  CONSTRAINT [FKg2chnekmjiaahkdp0rwa21wh6] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[active_error] CHECK CONSTRAINT [FKg2chnekmjiaahkdp0rwa21wh6]
GO
ALTER TABLE [dbo].[active_error]  WITH CHECK ADD  CONSTRAINT [FKi2xwx5yd8c6tjg31sjv0o3uxw] FOREIGN KEY([error_level_id])
REFERENCES [dbo].[error_level] ([error_level_id])
GO
ALTER TABLE [dbo].[active_error] CHECK CONSTRAINT [FKi2xwx5yd8c6tjg31sjv0o3uxw]
GO
ALTER TABLE [dbo].[active_error]  WITH CHECK ADD  CONSTRAINT [FKjawhl78i6ciffqbx4aau34lm9] FOREIGN KEY([error_id])
REFERENCES [dbo].[error] ([error_id])
GO
ALTER TABLE [dbo].[active_error] CHECK CONSTRAINT [FKjawhl78i6ciffqbx4aau34lm9]
GO
----------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[analog_in]  WITH CHECK ADD  CONSTRAINT [FK31011wqqx37ck2s2ndklo36f2] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[analog_in] CHECK CONSTRAINT [FK31011wqqx37ck2s2ndklo36f2]
GO
-----------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[balancing_data]  WITH CHECK ADD  CONSTRAINT [FKfrv40lmxpfci1a63h2gkerhmw] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[balancing_data] CHECK CONSTRAINT [FKfrv40lmxpfci1a63h2gkerhmw]
GO
----------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[closed_loop]  WITH CHECK ADD  CONSTRAINT [FK1hox3b0h2k7wdmsrgvg05h424] FOREIGN KEY([closed_loop_type_id])
REFERENCES [dbo].[closed_loop_type] ([closed_loop_type_id])
GO
ALTER TABLE [dbo].[closed_loop] CHECK CONSTRAINT [FK1hox3b0h2k7wdmsrgvg05h424]
GO
ALTER TABLE [dbo].[closed_loop]  WITH CHECK ADD  CONSTRAINT [FK8517tm1csilhoiyh6vv56j5y5] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[closed_loop] CHECK CONSTRAINT [FK8517tm1csilhoiyh6vv56j5y5]
GO
------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[digital_input]  WITH CHECK ADD  CONSTRAINT [FKn19n8k907775aepnbcyi7k8lv] FOREIGN KEY([digital_input_type_id])
REFERENCES [dbo].[digital_input_type] ([digital_input_type_id])
GO
ALTER TABLE [dbo].[digital_input] CHECK CONSTRAINT [FKn19n8k907775aepnbcyi7k8lv]
GO
ALTER TABLE [dbo].[digital_input]  WITH CHECK ADD  CONSTRAINT [FKolyms1f1uicogntqu1nq37uii] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[digital_input] CHECK CONSTRAINT [FKolyms1f1uicogntqu1nq37uii]
GO
-------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[digital_output]  WITH CHECK ADD  CONSTRAINT [FK6y4cv5edesu6hp4i6y09inv01] FOREIGN KEY([digital_output_type_id])
REFERENCES [dbo].[digital_output_type] ([digital_output_type_id])
GO
ALTER TABLE [dbo].[digital_output] CHECK CONSTRAINT [FK6y4cv5edesu6hp4i6y09inv01]
GO
ALTER TABLE [dbo].[digital_output]  WITH CHECK ADD  CONSTRAINT [FKt84rks0wyaay68t1h5ywfyxqw] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[digital_output] CHECK CONSTRAINT [FKt84rks0wyaay68t1h5ywfyxqw]
GO
---------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[iodata]  WITH CHECK ADD  CONSTRAINT [FK649sbx2m6398d1y0xpqaifwx7] FOREIGN KEY([pp_supply_mode_type_id])
REFERENCES [dbo].[pp_supply_mode_type] ([pp_supply_mode_type_id])
GO
ALTER TABLE [dbo].[iodata] CHECK CONSTRAINT [FK649sbx2m6398d1y0xpqaifwx7]
GO
ALTER TABLE [dbo].[iodata]  WITH CHECK ADD  CONSTRAINT [FK8ri60ievw96bpx4idkrtcg71o] FOREIGN KEY([heater_mode_type_id])
REFERENCES [dbo].[heater_mode_type] ([heater_mode_type_id])
GO
ALTER TABLE [dbo].[iodata] CHECK CONSTRAINT [FK8ri60ievw96bpx4idkrtcg71o]
GO
ALTER TABLE [dbo].[iodata]  WITH CHECK ADD  CONSTRAINT [FKelr794gusoh13s3x4fjm1fxth] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[iodata] CHECK CONSTRAINT [FKelr794gusoh13s3x4fjm1fxth]
GO
ALTER TABLE [dbo].[iodata]  WITH CHECK ADD  CONSTRAINT [FKp3gmh2xmju09gdrq4g8qx017k] FOREIGN KEY([pp_waste_mode_type_id])
REFERENCES [dbo].[pp_waste_mode_type] ([pp_waste_mode_type_id])
GO
ALTER TABLE [dbo].[iodata] CHECK CONSTRAINT [FKp3gmh2xmju09gdrq4g8qx017k]
GO
ALTER TABLE [dbo].[iodata]  WITH CHECK ADD  CONSTRAINT [FKqe1k39e221ekl8xi756d62xwy] FOREIGN KEY([gp_mode_type_id])
REFERENCES [dbo].[gp_mode_type] ([gp_mode_type_id])
GO
ALTER TABLE [dbo].[iodata] CHECK CONSTRAINT [FKqe1k39e221ekl8xi756d62xwy]
GO
--------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[machine_run]  WITH CHECK ADD  CONSTRAINT [FKk8v9phaug39g3tvilb0bdixv4] FOREIGN KEY([sond_data_config_id])
REFERENCES [dbo].[sond_data_configuration] ([sond_data_configuration_id])
GO
ALTER TABLE [dbo].[machine_run] CHECK CONSTRAINT [FKk8v9phaug39g3tvilb0bdixv4]
GO
-----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[machine_run_calibraton_data]  WITH CHECK ADD  CONSTRAINT [FK6n86yfci2kqq4du8mntkmed8q] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[machine_run_calibraton_data] CHECK CONSTRAINT [FK6n86yfci2kqq4du8mntkmed8q]
GO
ALTER TABLE [dbo].[machine_run_calibraton_data]  WITH CHECK ADD  CONSTRAINT [FK7mxvmo8pqa6tx6fgfqwxpug0] FOREIGN KEY([calibration_data_id])
REFERENCES [dbo].[calibration_data] ([calibration_data_id])
GO
ALTER TABLE [dbo].[machine_run_calibraton_data] CHECK CONSTRAINT [FK7mxvmo8pqa6tx6fgfqwxpug0]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[numeric_input]  WITH CHECK ADD  CONSTRAINT [FK5lcyvihnidg3kupwp4ul6199w] FOREIGN KEY([numeric_input_type_id])
REFERENCES [dbo].[numeric_input_type] ([numeric_input_type_id])
GO
ALTER TABLE [dbo].[numeric_input] CHECK CONSTRAINT [FK5lcyvihnidg3kupwp4ul6199w]
GO
ALTER TABLE [dbo].[numeric_input]  WITH CHECK ADD  CONSTRAINT [FKpn16f128a8lqpu8csxj4bcnkb] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[numeric_input] CHECK CONSTRAINT [FKpn16f128a8lqpu8csxj4bcnkb]
GO
-----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[numeric_output]  WITH CHECK ADD  CONSTRAINT [FK63990hhotkry8do7xpg1002vd] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[numeric_output] CHECK CONSTRAINT [FK63990hhotkry8do7xpg1002vd]
GO
ALTER TABLE [dbo].[numeric_output]  WITH CHECK ADD  CONSTRAINT [FKljgs1d2qy35h03htfgcy453xl] FOREIGN KEY([numeric_output_type_id])
REFERENCES [dbo].[numeric_output_type] ([numeric_output_type_id])
GO
ALTER TABLE [dbo].[numeric_output] CHECK CONSTRAINT [FKljgs1d2qy35h03htfgcy453xl]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[protect_state]  WITH CHECK ADD  CONSTRAINT [FKn03n0milir2fs603yjbsxyayu] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[protect_state] CHECK CONSTRAINT [FKn03n0milir2fs603yjbsxyayu]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[psconfiguration]  WITH CHECK ADD  CONSTRAINT [FK4vonpf4rl7eh5rkh6avju72ka] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[psconfiguration] CHECK CONSTRAINT [FK4vonpf4rl7eh5rkh6avju72ka]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[psmissed_message]  WITH CHECK ADD  CONSTRAINT [FK7nlgyv0n4570lj74ixmbngp9k] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[psmissed_message] CHECK CONSTRAINT [FK7nlgyv0n4570lj74ixmbngp9k]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[psstatus]  WITH CHECK ADD  CONSTRAINT [FKpdln380rlqafm0y3922bqo20d] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[psstatus] CHECK CONSTRAINT [FKpdln380rlqafm0y3922bqo20d]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[pump_data]  WITH CHECK ADD  CONSTRAINT [FKf2kiaf6j0n90na3xy9q4h9619] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[pump_data] CHECK CONSTRAINT [FKf2kiaf6j0n90na3xy9q4h9619]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[sond_data]  WITH CHECK ADD  CONSTRAINT [FKc2l5qlg5ij8mohjwwdxwlptm3] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[sond_data] CHECK CONSTRAINT [FKc2l5qlg5ij8mohjwwdxwlptm3]
GO
-----------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[system_diagnosis]  WITH CHECK ADD  CONSTRAINT [FKa7xm1obvb3cgiyf6k0uipnldm] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[system_diagnosis] CHECK CONSTRAINT [FKa7xm1obvb3cgiyf6k0uipnldm]
GO
------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE [dbo].[running_system_state]  WITH CHECK ADD  CONSTRAINT [FKcsy7o06a5n0bkg8a018b4dnc7] FOREIGN KEY([machine_run_id])
REFERENCES [dbo].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[running_system_state] CHECK CONSTRAINT [FKcsy7o06a5n0bkg8a018b4dnc7]
GO
ALTER TABLE [dbo].[running_system_state]  WITH CHECK ADD  CONSTRAINT [FKgtskbqtxasil8ara19uhm505t] FOREIGN KEY([system_state_node])
REFERENCES [dbo].[system_state] ([system_state_node])
GO
ALTER TABLE [dbo].[running_system_state] CHECK CONSTRAINT [FKgtskbqtxasil8ara19uhm505t]
GO
ALTER TABLE [dbo].[running_system_state]  WITH CHECK ADD  CONSTRAINT [FKqm0gwbrj1uqy679ntc1r521sl] FOREIGN KEY([subsequence_node])
REFERENCES [dbo].[system_state_subsequence] ([subsequence_node])
GO
ALTER TABLE [dbo].[running_system_state] CHECK CONSTRAINT [FKqm0gwbrj1uqy679ntc1r521sl]
GO
