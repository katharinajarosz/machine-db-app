USE [Machine_Data_DB];
GO



/****** Object:  Table [dbo].[catchphrase_translation]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[catchphrase_translation](
	[catchphrase_id] [int] IDENTITY(1,1) NOT NULL,
	[iso_369_3_id] [varchar](10) NOT NULL,
	[translation] [nvarchar](400) NOT NULL,
	[description] [nvarchar](400) NOT NULL,
 CONSTRAINT [PK_catchphrase_translation] PRIMARY KEY CLUSTERED 
(
	[catchphrase_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[audience]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[audience](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](400) NOT NULL,
 CONSTRAINT [PK_audience] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error](
	[id] [int] NOT NULL,
	[code] [nvarchar](16) NULL,
	[state] [nvarchar](200) NULL,
	[condition] [nvarchar](max) NULL,
	[severity_id] [int] NULL,
	[escalation] [int] NULL,
	[deprecated] [bit] NOT NULL,
	[comment] [varchar](600) NULL,
	[abortable] [int] NULL,
 CONSTRAINT [PK_error] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_catchphrase_translation]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_catchphrase_translation](
	[error_id] [int] NOT NULL,
	[catchphrase_id] [int] NOT NULL,
	[weight] [float] NULL,
	[comment] [nvarchar](400) NULL,
 CONSTRAINT [PK_errorcatchphrase] PRIMARY KEY CLUSTERED 
(
	[error_id] ASC,
	[catchphrase_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_description_translation]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_description_translation](
	[error_description_id] [int] IDENTITY(1,1) NOT NULL,
	[iso_369_3_id] [varchar](10) NOT NULL,
	[translation] [nvarchar](400) NOT NULL,
 CONSTRAINT [PK_error_description_translation] PRIMARY KEY CLUSTERED 
(
	[error_description_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_error_description_translation_audience]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_error_description_translation_audience](
	[error_id] [int] NOT NULL,
	[error_description_id] [int] NOT NULL,
	[audience_id] [int] NOT NULL,
 CONSTRAINT [PK_error_description_audience] PRIMARY KEY CLUSTERED 
(
	[error_id] ASC,
	[error_description_id] ASC,
	[audience_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_error_solution_translation_audience]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_error_solution_translation_audience](
	[error_id] [int] NOT NULL,
	[error_solution_id] [int] NOT NULL,
	[audience_id] [int] NOT NULL,
	[priority] [int] NOT NULL,
 CONSTRAINT [PK_error_solution_audience] PRIMARY KEY CLUSTERED 
(
	[error_id] ASC,
	[error_solution_id] ASC,
	[audience_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_lost_fields]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_lost_fields](
	[error_id] [int] NOT NULL,
	[Reviewed_von] [nvarchar](50) NULL,
	[Reviewed_am] [float] NULL,
	[Quittierbar] [nvarchar](1) NULL,
 CONSTRAINT [PK_error_lost_fields] PRIMARY KEY CLUSTERED 
(
	[error_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_severity]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_severity](
	[internal] [nvarchar](50) NOT NULL,
	[severity] [nvarchar](50) NOT NULL,
	[id] [int] NOT NULL,
 CONSTRAINT [PK_error_severity] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[error_solution_translation]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[error_solution_translation](
	[error_solution_id] [int] IDENTITY(1,1) NOT NULL,
	[iso_369_3_id] [varchar](10) NOT NULL,
	[translation] [nvarchar](400) NOT NULL,
	[comment] [nvarchar](400) NULL,
 CONSTRAINT [PK_error_solution_translation] PRIMARY KEY CLUSTERED 
(
	[error_solution_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[iso-639-3]    Script Date: 27.01.2020 17:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[iso-639-3](
	[Id] [varchar](10) NOT NULL,
	[Part2B] [varchar](10) NULL,
	[Part2T] [varchar](10) NULL,
	[Part1] [varchar](10) NULL,
	[Scope] [varchar](10) NOT NULL,
	[Language_Type] [varchar](10) NOT NULL,
	[Ref_Name] [nvarchar](100) NOT NULL,
	[Comment] [nvarchar](600) NULL,
 CONSTRAINT [PK_iso-639-3] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO





ALTER TABLE [dbo].[catchphrase_translation] ADD  CONSTRAINT [DF_catchphrase_translation_iso_369_3_id]  DEFAULT ('deu') FOR [iso_369_3_id]
GO
ALTER TABLE [dbo].[error] ADD  CONSTRAINT [DF_error_deprecated]  DEFAULT ((0)) FOR [deprecated]
GO
ALTER TABLE [dbo].[error_description_translation] ADD  CONSTRAINT [DF_error_description_translation_iso_369_3_id]  DEFAULT ('deu') FOR [iso_369_3_id]
GO
ALTER TABLE [dbo].[error_solution_translation] ADD  CONSTRAINT [DF_error_solution_translation_iso_369_3_id]  DEFAULT ('deu') FOR [iso_369_3_id]
GO
ALTER TABLE [dbo].[catchphrase_translation]  WITH CHECK ADD  CONSTRAINT [FK_catchphrase_translation_iso-639-3] FOREIGN KEY([iso_369_3_id])
REFERENCES [dbo].[iso-639-3] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[catchphrase_translation] CHECK CONSTRAINT [FK_catchphrase_translation_iso-639-3]
GO
ALTER TABLE [dbo].[error]  WITH CHECK ADD  CONSTRAINT [FK_error_error_severity] FOREIGN KEY([severity_id])
REFERENCES [dbo].[error_severity] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error] CHECK CONSTRAINT [FK_error_error_severity]
GO
ALTER TABLE [dbo].[error_catchphrase_translation]  WITH CHECK ADD  CONSTRAINT [FK_error_catchphrase_catchphrase_translation] FOREIGN KEY([catchphrase_id])
REFERENCES [dbo].[catchphrase_translation] ([catchphrase_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_catchphrase_translation] CHECK CONSTRAINT [FK_error_catchphrase_catchphrase_translation]
GO
ALTER TABLE [dbo].[error_catchphrase_translation]  WITH CHECK ADD  CONSTRAINT [FK_error_catchphrase_error] FOREIGN KEY([error_id])
REFERENCES [dbo].[error] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_catchphrase_translation] CHECK CONSTRAINT [FK_error_catchphrase_error]
GO
ALTER TABLE [dbo].[error_description_translation]  WITH CHECK ADD  CONSTRAINT [FK_error_description_translation_iso-639-3] FOREIGN KEY([iso_369_3_id])
REFERENCES [dbo].[iso-639-3] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_description_translation] CHECK CONSTRAINT [FK_error_description_translation_iso-639-3]
GO
ALTER TABLE [dbo].[error_error_description_translation_audience]  WITH CHECK ADD  CONSTRAINT [FK_error_description_audience_audience] FOREIGN KEY([audience_id])
REFERENCES [dbo].[audience] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_error_description_translation_audience] CHECK CONSTRAINT [FK_error_description_audience_audience]
GO
ALTER TABLE [dbo].[error_error_description_translation_audience]  WITH CHECK ADD  CONSTRAINT [FK_error_description_audience_error] FOREIGN KEY([error_id])
REFERENCES [dbo].[error] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_error_description_translation_audience] CHECK CONSTRAINT [FK_error_description_audience_error]
GO
ALTER TABLE [dbo].[error_error_description_translation_audience]  WITH CHECK ADD  CONSTRAINT [FK_error_description_audience_error_description_translation] FOREIGN KEY([error_description_id])
REFERENCES [dbo].[error_description_translation] ([error_description_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_error_description_translation_audience] CHECK CONSTRAINT [FK_error_description_audience_error_description_translation]
GO
ALTER TABLE [dbo].[error_error_solution_translation_audience]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_audience_audience] FOREIGN KEY([audience_id])
REFERENCES [dbo].[audience] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_error_solution_translation_audience] CHECK CONSTRAINT [FK_error_solution_audience_audience]
GO
ALTER TABLE [dbo].[error_error_solution_translation_audience]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_audience_error] FOREIGN KEY([error_id])
REFERENCES [dbo].[error] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_error_solution_translation_audience] CHECK CONSTRAINT [FK_error_solution_audience_error]
GO
ALTER TABLE [dbo].[error_error_solution_translation_audience]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_audience_error_solution_translation] FOREIGN KEY([error_solution_id])
REFERENCES [dbo].[error_solution_translation] ([error_solution_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_error_solution_translation_audience] CHECK CONSTRAINT [FK_error_solution_audience_error_solution_translation]
GO
ALTER TABLE [dbo].[error_lost_fields]  WITH CHECK ADD  CONSTRAINT [FK_error_lost_fields_error] FOREIGN KEY([error_id])
REFERENCES [dbo].[error] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_lost_fields] CHECK CONSTRAINT [FK_error_lost_fields_error]
GO
ALTER TABLE [dbo].[error_solution_translation]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_translation_iso-639-3] FOREIGN KEY([iso_369_3_id])
REFERENCES [dbo].[iso-639-3] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[error_solution_translation] CHECK CONSTRAINT [FK_error_solution_translation_iso-639-3]
GO
