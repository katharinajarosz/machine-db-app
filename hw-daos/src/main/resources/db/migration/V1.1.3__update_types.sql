 --------------------------------------------numeric_inputs-------------------------------------------

insert into dbo.numeric_input_type(identifier) values('S_VOLT_65_1');
insert into dbo.numeric_input_type(identifier) values('S_VOLT_65_2');
insert into dbo.numeric_input_type(identifier) values('S_VOLT_24M1');
insert into dbo.numeric_input_type(identifier) values('S_VOLT_24M2');
insert into dbo.numeric_input_type(identifier) values('S_VOLT_24M3');
insert into dbo.numeric_input_type(identifier) values('S_VOLT_24M1x');
insert into dbo.numeric_input_type(identifier) values('S_VOLT_5');
insert into dbo.numeric_input_type(identifier) values('S_P_HW_ACID');
insert into dbo.numeric_input_type(identifier) values('S_P_HW_BASE');
insert into dbo.numeric_input_type(identifier) values('S_P_DIA_OUT');
insert into dbo.numeric_input_type(identifier) values('S_P_DIA_IN');
insert into dbo.numeric_input_type(identifier) values('S_P_BLOOD_ART');
insert into dbo.numeric_input_type(identifier) values('S_P_BLOOD_PRE');
insert into dbo.numeric_input_type(identifier) values('S_P_BLOOD_VENOUS');
insert into dbo.numeric_input_type(identifier) values('S_P_BLOOD_ART_MON');
insert into dbo.numeric_input_type(identifier) values('S_T_HOUSING');
insert into dbo.numeric_input_type(identifier) values('S_T_EXTERN');
insert into dbo.numeric_input_type(identifier) values('S_SCALE_1');
insert into dbo.numeric_input_type(identifier) values('S_SCALE_2');
insert into dbo.numeric_input_type(identifier) values('S_SCALE_SUM');
insert into dbo.numeric_input_type(identifier) values('S_VEN_CLAMP_POSITION');
insert into dbo.numeric_input_type(identifier) values('S_ART_BLOOD');
insert into dbo.numeric_input_type(identifier) values('S_BLOOD_PUMP_FB');
insert into dbo.numeric_input_type(identifier) values('S_PRE_DILUTION_PUMP_FB');
insert into dbo.numeric_input_type(identifier) values('S_GP_HW_ACID_FB');
insert into dbo.numeric_input_type(identifier) values('S_GP_HW_BASE_FB');
insert into dbo.numeric_input_type(identifier) values('S_GP_DIA_FB');
insert into dbo.numeric_input_type(identifier) values('S_PP_CONC_ACID_FB');
insert into dbo.numeric_input_type(identifier) values('S_PP_CONC_BASE_FB');
insert into dbo.numeric_input_type(identifier) values('S_PP_SU_ACID_FB');
insert into dbo.numeric_input_type(identifier) values('S_PP_SU_BASE_FB');
insert into dbo.numeric_input_type(identifier) values('S_PP_WA_A_FB');
insert into dbo.numeric_input_type(identifier) values('S_PP_WA_B_FB');
insert into dbo.numeric_input_type(identifier) values('S_F_DIA');
insert into dbo.numeric_input_type(identifier) values('S_F_HW_ACID');
insert into dbo.numeric_input_type(identifier) values('S_F_HW_BASE');
insert into dbo.numeric_input_type(identifier) values('S_F_RO_WATER');
insert into dbo.numeric_input_type(identifier) values('S_BLD');
insert into dbo.numeric_input_type(identifier) values('S_GP_HW_ACID_CNT');
insert into dbo.numeric_input_type(identifier) values('S_GP_HW_BASE_CNT');
insert into dbo.numeric_input_type(identifier) values('S_GP_DIA_CNT');
insert into dbo.numeric_input_type(identifier) values('S_BLOOD_PUMP_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PRE_DILUTION_PUMP_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PP_SU_ACID_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PP_SU_BASE_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PP_WA_A_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PP_WA_B_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PP_CONC_ACID_CNT');
insert into dbo.numeric_input_type(identifier) values('S_PP_CONC_BASE_CNT');
insert into dbo.numeric_input_type(identifier) values('S_CONT_MOTOR_FB');
insert into dbo.numeric_input_type(identifier) values('S_CONTROL_BOARD_TEMPERATURE');
insert into dbo.numeric_input_type(identifier) values('C_T_ACID');
insert into dbo.numeric_input_type(identifier) values('C_T_BASE');
insert into dbo.numeric_input_type(identifier) values('C_T_MON');
insert into dbo.numeric_input_type(identifier) values('C_T_RES');
insert into dbo.numeric_input_type(identifier) values('C_PH_ACID');
insert into dbo.numeric_input_type(identifier) values('C_PH_BASE');
insert into dbo.numeric_input_type(identifier) values('C_PH_MON');
insert into dbo.numeric_input_type(identifier) values('C_PH_RES');
insert into dbo.numeric_input_type(identifier) values('C_TMP');
insert into dbo.numeric_input_type(identifier) values('C_F_SU_BASE');
insert into dbo.numeric_input_type(identifier) values('C_P_SYSTEM');
insert into dbo.numeric_input_type(identifier) values('C_UF_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_PRE_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_RO_VOLUME_FLOW');
insert into dbo.numeric_input_type(identifier) values('C_RO_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_WASTE_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_ACID_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_BASE_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_BLOOD_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_EFF_BLOOD_VOLUME');
insert into dbo.numeric_input_type(identifier) values('C_GP_HW_ACID_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_GP_HW_ACID_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_GP_HW_BASE_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_GP_HW_BASE_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_GP_DIA_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_GP_DIA_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_BLOOD_PUMP_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_BLOOD_PUMP_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PRE_DILUTION_PUMP_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PRE_DILUTION_PUMP_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PP_SU_ACID_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PP_SU_ACID_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PP_SU_BASE_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PP_SU_BASE_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PP_WA_A_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PP_WA_A_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PP_WA_B_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PP_WA_B_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PP_CONC_ACID_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PP_CONC_ACID_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_PP_CONC_BASE_OPERATING_HOURS');
insert into dbo.numeric_input_type(identifier) values('C_PP_CONC_BASE_OPERATING_CYCLES');
insert into dbo.numeric_input_type(identifier) values('C_SUPPLY_VOLUME');

--------------------------------------------digital_inputs-------------------------------------------

insert into dbo.digital_input_type(identifier) values('S_KEYBOARD_1');
insert into dbo.digital_input_type(identifier) values('S_KEYBOARD_2');
insert into dbo.digital_input_type(identifier) values('S_SERVICE_SWITCH');
insert into dbo.digital_input_type(identifier) values('S_DOOR_BLOOD_PUMP');
insert into dbo.digital_input_type(identifier) values('S_DOOR_PRE_DILUTION_PUMP');
insert into dbo.digital_input_type(identifier) values('S_BS_AIR_PRE');
insert into dbo.digital_input_type(identifier) values('S_BS_AIR_CONC_ACID');
insert into dbo.digital_input_type(identifier) values('S_BS_AIR_CONC_BASE');
insert into dbo.digital_input_type(identifier) values('S_OVERPRESSURE_WASTE');
insert into dbo.digital_input_type(identifier) values('S_VEN_BUBBLE_1');
insert into dbo.digital_input_type(identifier) values('S_FCC_1');
insert into dbo.digital_input_type(identifier) values('S_FCC_2');
insert into dbo.digital_input_type(identifier) values('S_HEATER_HW_FB');
insert into dbo.digital_input_type(identifier) values('S_HEATER_BASE_FB');
insert into dbo.digital_input_type(identifier) values('S_HEATER_SAFETY_RELAIS_FB');
insert into dbo.digital_input_type(identifier) values('S_ALARM_PROTECT');
insert into dbo.digital_input_type(identifier) values('S_HANSEN_1');
insert into dbo.digital_input_type(identifier) values('S_HANSEN_2');
insert into dbo.digital_input_type(identifier) values('C_PATIENT_CONNECTED');
insert into dbo.digital_input_type(identifier) values('S_CONT_MOTOR_UP');
insert into dbo.digital_input_type(identifier) values('S_CONT_MOTOR_DOWN');
insert into dbo.digital_input_type(identifier) values('S_BS_RES_CROSS_0');
insert into dbo.digital_input_type(identifier) values('S_BS_RES_CROSS_1');
insert into dbo.digital_input_type(identifier) values('RT_ACTIVE');

--------------------------------------------closed_loops-------------------------------------------

insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PP_SU_ACID');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PP_SU_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PP_CONC_ACID');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PP_CONC_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_BLOOD_PUMP');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PRE_DILUTION_PUMP');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_UF_PUMP');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_DIA');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_HW_ACID');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_HW_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_SPEED_DIA');
insert into dbo.closed_loop_type(identifier) values('CL_SPEED_HW_ACID');
insert into dbo.closed_loop_type(identifier) values('CL_SPEED_HW_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PP_WA_A');
insert into dbo.closed_loop_type(identifier) values('CL_FLOW_PP_WA_B');
insert into dbo.closed_loop_type(identifier) values('CL_BALANCE_PP_WASTE');
insert into dbo.closed_loop_type(identifier) values('CL_PRESSURE_PP_WASTE');
insert into dbo.closed_loop_type(identifier) values('CL_TREATMENT_PRESSURE_PP_WASTE');
insert into dbo.closed_loop_type(identifier) values('CL_PERCENTAGE_HEATER_HW');
insert into dbo.closed_loop_type(identifier) values('CL_PERCENTAGE_HEATER_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_HEATER_HW');
insert into dbo.closed_loop_type(identifier) values('CL_HEATER_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_SUPPLY_FLOW');
insert into dbo.closed_loop_type(identifier) values('CL_SUPPLY_REL');
insert into dbo.closed_loop_type(identifier) values('CL_PH_ACID');
insert into dbo.closed_loop_type(identifier) values('CL_PH_BASE');
insert into dbo.closed_loop_type(identifier) values('CL_PH_RES');

--------------------------------------------numeric_outputs-------------------------------------------

insert into dbo.numeric_output_type(identifier) values('A_BLOOD_PUMP');
insert into dbo.numeric_output_type(identifier) values('A_PRE_DILUTION_PUMP');
insert into dbo.numeric_output_type(identifier) values('A_GP_DIA');
insert into dbo.numeric_output_type(identifier) values('A_GP_HW_ACID');
insert into dbo.numeric_output_type(identifier) values('A_GP_HW_BASE');
insert into dbo.numeric_output_type(identifier) values('A_PP_SU_ACID');
insert into dbo.numeric_output_type(identifier) values('A_PP_SU_BASE');
insert into dbo.numeric_output_type(identifier) values('A_PP_WA_A');
insert into dbo.numeric_output_type(identifier) values('A_PP_WA_B');
insert into dbo.numeric_output_type(identifier) values('A_PP_CONC_ACID');
insert into dbo.numeric_output_type(identifier) values('A_PP_CONC_BASE');
insert into dbo.numeric_output_type(identifier) values('A_HEATER_HW');
insert into dbo.numeric_output_type(identifier) values('A_HEATER_BASE');

--------------------------------------------digital_outputs-------------------------------------------

insert into dbo.digital_output_type(identifier) values('A_LED_KEY_1');
insert into dbo.digital_output_type(identifier) values('A_LED_KEY_2');
insert into dbo.digital_output_type(identifier) values('A_CLAMP_VEN');
insert into dbo.digital_output_type(identifier) values('A_INFO_GREEN');
insert into dbo.digital_output_type(identifier) values('A_INFO_YELLOW');
insert into dbo.digital_output_type(identifier) values('A_INFO_RED');
insert into dbo.digital_output_type(identifier) values('A_ART_BLOOD');
insert into dbo.digital_output_type(identifier) values('A_BLD');
insert into dbo.digital_output_type(identifier) values('A_BUZZER');
insert into dbo.digital_output_type(identifier) values('A_V3_DIA_OUT');
insert into dbo.digital_output_type(identifier) values('A_V2_DIA_IN');
insert into dbo.digital_output_type(identifier) values('A_V3_CROSS');
insert into dbo.digital_output_type(identifier) values('A_V3_AIRSWITCH');
insert into dbo.digital_output_type(identifier) values('A_V3_AIR_B');
insert into dbo.digital_output_type(identifier) values('A_V3_EMPTY');
insert into dbo.digital_output_type(identifier) values('A_V3_AIR_A');
insert into dbo.digital_output_type(identifier) values('A_V3_DIS_CIT');
insert into dbo.digital_output_type(identifier) values('A_V3_WASTE');
insert into dbo.digital_output_type(identifier) values('A_CONT_MOTOR_PWR');
insert into dbo.digital_output_type(identifier) values('A_CONT_MOTOR_DIR');
insert into dbo.digital_output_type(identifier) values('A_FAN_SWITCH');
insert into dbo.digital_output_type(identifier) values('RT-FPGA_WATCHDOG');

--------------------------------------------pp_supply_mode-------------------------------------------

insert into dbo.pp_supply_mode_type(identifier) values('Independent');
insert into dbo.pp_supply_mode_type(identifier) values('Linked');

--------------------------------------------pp_waste_mode-------------------------------------------

insert into dbo.pp_waste_mode_type(identifier) values('Flow');
insert into dbo.pp_waste_mode_type(identifier) values('Balance');
insert into dbo.pp_waste_mode_type(identifier) values('Pressure');
insert into dbo.pp_waste_mode_type(identifier) values('TreatmentPressure');

--------------------------------------------gp_mode-------------------------------------------

insert into dbo.gp_mode_type(identifier) values('Speed');
insert into dbo.gp_mode_type(identifier) values('Flow');
insert into dbo.gp_mode_type(identifier) values('pH');

--------------------------------------------heater_mode-------------------------------------------

insert into dbo.heater_mode_type(identifier) values('Percenatage');
insert into dbo.heater_mode_type(identifier) values('TemperatureAutomatic');

--------------------------------------------error_level-------------------------------------------

insert into dbo.error_level(identifier,severity) values('nothing','Nothing');
insert into dbo.error_level(identifier,severity) values('hidden info','Info');
insert into dbo.error_level(identifier,severity) values('NI Error','Info');
insert into dbo.error_level(identifier,severity) values('no alarm tone','Info');
insert into dbo.error_level(identifier,severity) values('info tone','Info');
insert into dbo.error_level(identifier,severity) values('low priority alarm','Warning');
insert into dbo.error_level(identifier,severity) values('high priority alarm','Error');
insert into dbo.error_level(identifier,severity) values('protective system alarm','Fatal');

