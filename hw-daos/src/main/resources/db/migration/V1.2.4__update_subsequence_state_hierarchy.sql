IF OBJECT_ID('AddSubsequenceState','P') IS NOT NULL
   DROP PROCEDURE AddSubsequenceState
GO

CREATE PROC AddSubsequenceState(@stateId int OUTPUT, @stateName varchar(50))   
AS   
BEGIN  
   DECLARE @parentNode hierarchyid, @lastChildNode hierarchyid

   DECLARE ParentCursor CURSOR FOR
   SELECT subsequence_node
   FROM machine.system_state_subsequence
   WHERE subsequence_level = 1

   OPEN ParentCursor

   FETCH NEXT FROM ParentCursor INTO @parentNode

   WHILE(@@FETCH_STATUS = 0)
   
   BEGIN  
		
      SELECT @lastChildNode = max(subsequence_node)
		FROM machine.system_state_subsequence
		WHERE subsequence_node.GetAncestor(1) = @parentNode
	  INSERT machine.system_state_subsequence (subsequence_node, subsequence_id, subsequence_name)  
      VALUES(@parentNode.GetDescendant(@lastChildNode, NULL), @stateId, @stateName)
	  FETCH NEXT FROM ParentCursor INTO @parentNode

   END  
CLOSE ParentCursor
DEALLOCATE ParentCursor
SET @stateId = @stateId+1;  
END
GO

DECLARE @stateId int = 0

EXEC AddSubsequenceState @stateId OUTPUT,'Step_1';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_2';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_3';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_4';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_5';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_6';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_7';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_8';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_9';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_10';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_11';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_12';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_13';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_14';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_15';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_16';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_17';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_18';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_19';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_20';
EXEC AddSubsequenceState @stateId OUTPUT,'Step_exit';
GO

IF OBJECT_ID('AddSubsequenceState','P') IS NOT NULL
   DROP PROCEDURE AddSubsequenceState
GO