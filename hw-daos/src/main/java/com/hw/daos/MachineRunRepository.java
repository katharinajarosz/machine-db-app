package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.globaldata.MachineRun;

/**
 * The extension of {@link JpaRepository} for {@link MachineRun}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface MachineRunRepository extends JpaRepository<MachineRun, Long> {
		 
	
	MachineRun findByFileName(String fileName);

}
