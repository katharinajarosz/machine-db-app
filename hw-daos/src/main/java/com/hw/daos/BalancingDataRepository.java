package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.machinedataset.BalancingData;
import com.hw.database.beans.machinedataset.BalancingDataIdentity;

/**
 * The extension of {@link JpaRepository} for {@link BalancingData}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface BalancingDataRepository extends JpaRepository<BalancingData, Long> {

}
