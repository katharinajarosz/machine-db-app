package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.DigitalInput;
import com.hw.database.beans.controlsystem.DigitalInputIdentity;

/**
 * The extension of {@link JpaRepository} for {@link DigitalInput}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface DigitalInputRepository extends JpaRepository<DigitalInput, Long> {
	
	

}
