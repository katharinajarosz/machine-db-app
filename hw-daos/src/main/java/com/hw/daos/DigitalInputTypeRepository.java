package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.DigitalInputType;

/**
 * The extension of {@link JpaRepository} for {@link DigitalInputType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface DigitalInputTypeRepository extends JpaRepository<DigitalInputType, Integer> {
	
	DigitalInputType findById(int id);

}
