package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.protectivesystem.PSStatus;

/**
 * The extension of {@link JpaRepository} for {@link PSStatus}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface PsStatusRepository extends JpaRepository<PSStatus, Long> {
	
	

}
