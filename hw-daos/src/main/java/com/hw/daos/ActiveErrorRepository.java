package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.errordata.ActiveError;

/**
 * The extension of {@link JpaRepository} for {@link ActiveError}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface ActiveErrorRepository extends JpaRepository<ActiveError, Long> {
	
	ActiveError findByActiveErrorId(long errorId);	

}
