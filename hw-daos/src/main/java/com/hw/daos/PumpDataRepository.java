package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.protectivesystem.PumpData;
import com.hw.database.beans.protectivesystem.PumpDataIdentity;

/**
 * The extension of {@link JpaRepository} for {@link PumpData}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface PumpDataRepository extends JpaRepository<PumpData, Long> {

}
