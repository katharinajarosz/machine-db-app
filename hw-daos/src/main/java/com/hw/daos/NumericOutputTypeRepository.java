package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.NumericOutputType;

/**
 * The extension of {@link JpaRepository} for {@link NumericOutputType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface NumericOutputTypeRepository extends JpaRepository<NumericOutputType, Integer> {
	
	NumericOutputType findById(int id);

}
