package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.errordata.Error;

/**
 * The extension of {@link JpaRepository} for {@link Error}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface ErrorRepository extends JpaRepository<Error, Integer> {
	
	Error findByErrorId(int errorId);	

}
