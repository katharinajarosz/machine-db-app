package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.IOData;

/**
 * The extension of {@link JpaRepository} for {@link IOData}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface IoDataRepository extends JpaRepository<IOData, Long> {
	
	

}
