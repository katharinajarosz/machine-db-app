package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.protectivesystem.SondData;
import com.hw.database.beans.protectivesystem.SondDataIdentity;

/**
 * The extension of {@link JpaRepository} for {@link SondData}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface SondDataRepository extends JpaRepository<SondData, Long> {

}
