package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.protectivesystem.AnalogIn;
import com.hw.database.beans.protectivesystem.AnalogInIdentity;

/**
 * The extension of {@link JpaRepository} for {@link AnalogIn}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface AnalogInRepository extends JpaRepository<AnalogIn, Long> {

}
