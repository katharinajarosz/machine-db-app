package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.DigitalOutput;
import com.hw.database.beans.controlsystem.DigitalOutputIdentity;

/**
 * The extension of {@link JpaRepository} for {@link DigitalOutput}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface DigitalOutputRepository extends JpaRepository<DigitalOutput, Long> {
	
}
