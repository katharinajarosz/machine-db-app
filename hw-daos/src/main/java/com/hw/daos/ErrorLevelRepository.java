package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.errordata.ErrorLevel;

/**
 * The extension of {@link JpaRepository} for {@link ErrorLevel}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface ErrorLevelRepository extends JpaRepository<ErrorLevel, Integer> {
	
	
	
	ErrorLevel findByErrorLevelId(int errorLevelId);

}
