package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.PpSupplyModeType;

/**
 * The extension of {@link JpaRepository} for {@link PpSupplyModeType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface PpSupplyModeTypeRepository extends JpaRepository<PpSupplyModeType, Integer> {
	
	PpSupplyModeType findById(int id);

}
