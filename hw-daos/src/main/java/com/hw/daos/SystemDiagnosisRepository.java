package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.machinedataset.SystemDiagnosis;
import com.hw.database.beans.machinedataset.SystemDiagnosisIdentity;

/**
 * The extension of {@link JpaRepository} for {@link SystemDiagnosis}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface SystemDiagnosisRepository extends JpaRepository<SystemDiagnosis, Long> {

}
