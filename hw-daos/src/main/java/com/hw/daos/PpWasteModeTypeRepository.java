package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.PpWasteModeType;

/**
 * The extension of {@link JpaRepository} for {@link PpWasteModeType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface PpWasteModeTypeRepository extends JpaRepository<PpWasteModeType, Integer> {
	
	PpWasteModeType findById(int id);

}
