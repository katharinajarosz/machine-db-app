/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The sond data process image bean.
 * <p>
 * This bean contains 2 {@code short} data types and 2 {@code float} data types
 * and the sond names as {@code Enum}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RSondData {

	private short upH;
	
	private short uTemp;
	
	private float temperature;
	
	private float ph;


}
