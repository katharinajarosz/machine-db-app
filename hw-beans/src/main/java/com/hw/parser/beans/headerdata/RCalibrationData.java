/**
 * 
 */
package com.hw.parser.beans.headerdata;

import java.util.List;

import javax.annotation.ManagedBean;

import com.hw.database.beans.globaldata.CalibrationData.CalibrationType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The calibration data process image bean.
 * <p>
 * This bean contains a section size as {@code int}, a number of entries as
 * {@code int} and a set of {@link CalibrationType} and {@code Double}'s as a
 * {@code Map}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ManagedBean
public class RCalibrationData {

	
	private int sectionSize;
	
	private int numberOfEntries;
	
	private List<Double> calibration;


}
