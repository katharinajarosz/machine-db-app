/**
 * 
 */
package com.hw.parser.beans;

import javax.annotation.ManagedBean;

import com.hw.parser.beans.headerdata.RCalibrationData;
import com.hw.parser.beans.headerdata.RFileHeader;
import com.hw.parser.beans.headerdata.RSystemConfiguration;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The header data process image bean.
 * <p>
 * This bean contains the {@link RFileHeader}, the {@link RSystemConfiguration}
 * and the {@link RCalibrationData}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RHeaderData {

	private RFileHeader fileHeader;
	
	private RSystemConfiguration systemConfiguration;
	
	private RCalibrationData calibrationData;

}
