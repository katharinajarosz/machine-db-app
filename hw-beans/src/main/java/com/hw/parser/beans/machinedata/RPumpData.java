/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The pump data process image bean.
 * <p>
 * This bean contains 3 {@code float} data types and the pump names as
 * {@code Enum}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RPumpData {

	private float host;
	
	private float control;
	
	private float meas;


}
