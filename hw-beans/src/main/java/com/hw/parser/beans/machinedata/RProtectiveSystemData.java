/**
 * 
 */
package com.hw.parser.beans.machinedata;

import java.util.List;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The protective system data process image bean.
 * <p>
 * This bean contains 4 {@code int} data types, 9 {@code byte} data types,
 * {@link RSondData} as a {@code Map}, {@link RPumpData} as a {@code Map},
 * {@link RAnalogIn} as a {@code Map} and {@link RBalancingData}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RProtectiveSystemData {

	
	private int unknownMessageCount;
	
	private int crcErrorCount;
	
	private int messageCounterErrorCount;
	
	private byte sendMsgCounter;
	
	private byte protectState;
	
	private byte protectSubstate;
	
	private byte controlStatusByte;
	
	private byte protectStatusByte;

	private List<RSondData> sondData;

	private List<RPumpData> pumpData;
	
	private List<RAnalogIn> analogIn;
	
	private byte lastReceivedMessageID;
	
	private byte lastSendMessageID;
	
	private byte lastSendMainState;
	
	private byte lastSendSubState;
	
	private int rs232CommunicationLoopExecutionTime;

	private RBalancingData balancingData;

}
