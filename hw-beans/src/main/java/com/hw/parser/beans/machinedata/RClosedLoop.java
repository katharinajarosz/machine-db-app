/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The closed loops data process image bean.
 * <p>
 * This bean contains an {@code int} that represents the enum of the current
 * closed loop and 6 {@code float} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RClosedLoop {
	
	private int closedLoop;
	
	private float hmi;
	
	private float proc;
	
	private float dem;
	
	private float y;
	
	private float inp;
	
	private float e;

}
