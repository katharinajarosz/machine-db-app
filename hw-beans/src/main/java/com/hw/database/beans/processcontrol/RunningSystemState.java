package com.hw.database.beans.processcontrol;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The system state data base bean.
 * <p>
 * Contain a bidirectional relationship to {@link MachineRun} 
 * and an unidirectional relationship to {@link SystemStates}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "RunningSystemState", schema = "machine")
@ManagedBean
public class RunningSystemState {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Running_System_State_ID")
	private Long runningSystemStateID;
	
	@Column(name = "Start")
	private Timestamp start;
	
	@Column(name = "Ende")
	private Timestamp end;
	
//	@ManyToOne
//	@JoinColumn(name = "System_States_ID")
//	private SystemStates systemState;
	
	@ManyToOne
	@JoinColumn(name = "System_State_Node")
	private SystemState systemStateNode;
	
	@ManyToOne
	@JoinColumn(name = "Subsequence_Node")
	private SystemStateSubsequence subsequenceNode;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
}
