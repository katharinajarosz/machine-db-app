package com.hw.database.beans.protectivesystem;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The acid/base configuration data base bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineRun}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "PSConfiguration", schema = "machine")
@ManagedBean
public class PSConfiguration {
	
	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "PS_Configuration_ID")
	private long id;
	
	@Column(name ="Acid_Density")
	private float acidDensity;
	
	@Column(name ="Base_Density")
	private float baseDensity;
	
	@MapsId
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;

}
