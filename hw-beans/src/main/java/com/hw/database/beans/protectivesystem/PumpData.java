/**
 * 
 */
package com.hw.database.beans.protectivesystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.protectivesystem.PumpData.PumpDataType;
import com.hw.parser.beans.machinedata.RPumpData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The pump data data base bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineDataSet}
 * 
 * @author kaja
 * @version 1.0
 * @see RPumpData
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "PumpData", schema = "machine")
@ManagedBean
public class PumpData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PumpDataId")
	private long id;
	
	@Column(name ="Host")
	private float host;
	
	@Column(name ="Control")
	private float control;
	
	@Column(name ="Measure")
	private float meas;	
	
	@Column(name = "Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@Column(name = "Type")
	@Convert(converter = PumpDataConverter.class)
	private PumpDataType type;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	

	/**
	 * The pump data enum.
	 * <p>
	 * This enum contains the pump names.
	 * 
	 * @author kaja
	 * @version 1.0
	 */
	
	public enum PumpDataType {
		BLOOD_PUMP("Blood Pump"), PREDILUTION_PUMP("Predilution Pump");
		
		private final String type;

		PumpDataType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public static PumpDataType fromType(String type) {
			for (PumpDataType pumpDataType : PumpDataType.values()) {
				if (pumpDataType.getType().equals(type)) {
					return pumpDataType;
				}
			}
			throw new UnsupportedOperationException("The type " + type + " is not supported!");
		}
	}

}
