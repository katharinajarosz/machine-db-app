package com.hw.database.beans.controlsystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.controlsystem.types.DigitalInputType;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RDigitalInput;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The digital input data base bean.
 * <p>
 * Contain a bidirectional relationship to {@link MachineDataSet} and an 
 * unidirectional relationship to {@link DigitalInputType}.
 * 
 * @author kaja
 * @version 1.0
 * @see RDigitalInput
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "DigitalInput", schema = "machine")
@ManagedBean
public class DigitalInput {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DigitalInputId")
	private long id;
	
	@Column(name = "CD")
	private boolean cd;
	
	@Column(name = "R")
	private boolean r;
	
	@ManyToOne
	@JoinColumn(name = "Digital_Input_Type_ID")
	private DigitalInputType type;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@Column(name = "Timestamp")
	private Timestamp machineDataLoopTimestamp;
	

}
