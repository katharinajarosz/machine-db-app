package com.hw.database.beans.protectivesystem;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.protectivesystem.PumpData.PumpDataType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@SuppressWarnings("serial")
//@Embeddable
//@Data
//@Builder @AllArgsConstructor
public class PumpDataIdentity implements Serializable {

//	@NotNull
//	@ManyToOne
//	@JoinColumn(name = "Machine_Run_ID")
//	private MachineRun machineRun;
//	@NotNull
//	private Timestamp machineDataLoopTimestamp;
//	@NotNull
//	@Column(name = "Type")
//	@Convert(converter = PumpDataConverter.class)
//	private PumpDataType type;

//	public PumpDataIdentity(MachineRun machineRun, LocalDateTime timestamp, PumpDataType type) {
//		this.machineRun = machineRun;
//		this.machineDataLoopTimestamp = timestamp;
//		this.type = type;
//	}
	
}