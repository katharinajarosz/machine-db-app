package com.hw.database.beans.errordata;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An active error data base bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineRun} and unidirectional relationships to {@link Error} 
 * and {@link ErrorLevel}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "ActiveError", schema = "error")
@ManagedBean
public class ActiveError {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Active_Error_ID")
	private long activeErrorId;
	
	@Column(name = "Start")
	private Timestamp start;
	
	@Column(name = "Ende")
	private Timestamp end;
	
	@ManyToOne
	@JoinColumn(name = "Error_Level_ID")
	private ErrorLevel errorLevel;
	
	@ManyToOne
	@JoinColumn(name = "Error_ID")
	private Error errorId;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
}
