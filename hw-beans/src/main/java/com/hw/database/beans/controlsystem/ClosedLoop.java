package com.hw.database.beans.controlsystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.controlsystem.types.ClosedLoopType;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RClosedLoop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The closed loop data base bean.
 * <p>
 * Contain a bidirectional relationship to {@link MachineDataSet} and an unidirectional relationship
 * to {@link ClosedLoopType}.
 * 
 * @author kaja
 * @version 1.0
 * @see RClosedLoop
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "ClosedLoop", schema = "machine")
@ManagedBean
public class ClosedLoop {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ClosedLoopId")
	private long id;
	
	@Column(name = "HMI")
	private float hmi;
	
	@Column(name = "PROCX")
	private float proc;
	
	@Column(name = "DEM")
	private float dem;
	
	@Column(name = "Y")
	private float y;
	
	@Column(name = "INP")
	private float inp;
	
	@Column(name = "E")
	private float e;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRunId;
	
	@Column(name = "Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@ManyToOne
	@JoinColumn(name = "Closed_Loop_Type_ID")
	private ClosedLoopType type;
	
	
}
