package com.hw.database.beans.globaldata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.hw.parser.beans.headerdata.RCalibrationData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The calibration data data base bean.
 * 
 * @author kaja
 * @version 1.0
 * @see RCalibrationData
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "CalibrationData", schema = "machine")
@ManagedBean
public class CalibrationData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Calibration_Data_ID")
	private long ID;
	
	@Convert(converter = CalibrationDataConverter.class)
	private CalibrationType type;
	
	@Column(name = "Value")
	private double value;
	
/**
 * The calibration data enum.
 * 
 * @author kaja
 *
 */
	public enum CalibrationType {
		
		S_P_DIA_IN_SC_OF("S_P_DIA_IN_SC_OF"),		
		S_P_DIA_IN_SC_G("S_P_DIA_IN_SC_G"),		
		S_P_DIA_OUT_SC_OF("S_P_DIA_OUT_SC_OF"),		
		S_P_DIA_OUT_SC_G("S_P_DIA_OUT_SC_G"),		
		S_P_HW_ACID_SC_OF("S_P_HW_ACID_SC_OF"),		
		S_P_HW_ACID_SC_G("S_P_HW_ACID_SC_G"),		
		S_P_HW_BASE_SC_OF("S_P_HW_BASE_SC_OF"),		
		S_P_HW_BASE_SC_G("S_P_HW_BASE_SC_G"),		
		S_P_BLOOD_ART_SC_OF("S_P_BLOOD_ART_SC_OF"),		
		S_P_BLOOD_ART_SC_G("S_P_BLOOD_ART_SC_G"),		
		S_P_BLOOD_ART_MON_SC_OF("S_P_BLOOD_ART_MON_SC_OF"),		
		S_P_BLOOD_ART_MON_SC_G("S_P_BLOOD_ART_MON_SC_G"),		
		S_P_BLOOD_PRE_SC_OF("S_P_BLOOD_PRE_SC_OF"),		
		S_P_BLOOD_PRE_SC_G("S_P_BLOOD_PRE_SC_G"),		
		S_P_BLOOD_VENOUS_SC_OF("S_P_BLOOD_VENOUS_SC_OF"),		
		S_P_BLOOD_VENOUS_SC_G("S_P_BLOOD_VENOUS_SC_G"),		
		S_F_HW_ACID_SC_G("S_F_HW_ACID_SC_G"),		
		S_F_HW_BASE_SC_G("S_F_HW_BASE_SC_G"),		
		CL_FLOW_PP_SU_ACID_SC_G("CL_FLOW_PP_SU_ACID_SC_G"),		
		CL_FLOW_PP_SU_BASE_SC_G("CL_FLOW_PP_SU_BASE_SC_G"),		
		CL_FLOW_PP_CONC_ACID_SC_G("CL_FLOW_PP_CONC_ACID_SC_G"),		
		CL_FLOW_PP_CONC_BASE_SC_G("CL_FLOW_PP_CONC_BASE_SC_G"),		
		CL_FLOW_PP_WA_A_SC_G("CL_FLOW_PP_WA_A_SC_G"),		
		CL_FLOW_PP_WA_B_SC_G("CL_FLOW_PP_WA_B_SC_G"),		
		CL_FLOW_PRE_DILUTION_PUMP_SC_G("CL_FLOW_PRE_DILUTION_PUMP_SC_G"),		
		CL_FLOW_BLOOD_PUMP_SC_G("CL_FLOW_BLOOD_PUMP_SC_G");
		

		private final String type;

		CalibrationType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public static CalibrationType fromType(String type) {
			for (CalibrationType cd : CalibrationType.values()) {
				if (cd.getType().equals(type)) {
					return cd;
				}
			}
			throw new UnsupportedOperationException("The type " + type + " is not supported!");
		}

	}

}
