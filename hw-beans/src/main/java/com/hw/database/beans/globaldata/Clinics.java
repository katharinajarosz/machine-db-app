package com.hw.database.beans.globaldata;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The clinic data base bean.
 * <p>
 * Contains an unidirectional relationship to {@link Address}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
// @Entity
// @ManagedBean
public class Clinics {
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "Clinic_ID")
	private long ID;
	
	// @Column(name = "Clinic_Name")
	private String clinicName;
	
//	@OneToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "ID_Address_Data")
	private Address addr;
	
}
