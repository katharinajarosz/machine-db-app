/**
 * 
 */
package com.hw.database.beans.protectivesystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.protectivesystem.AnalogIn.AnalogInType;
import com.hw.parser.beans.machinedata.RAnalogIn;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


/**
 * The analog in data base bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineDataSet}.
 * 
 * @author kaja
 * @version 1.0
 * @see RAnalogIn
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "AnalogIn", schema = "machine")
@ManagedBean
public class AnalogIn {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "AnalogInId")
	private long id;
	
	@Column(name = "Filtered_Data")
	private float f;
	
	@Column(name = "Converted_Data")
	private float cd;
	
	@Column(name = "Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@Column(name = "Type")
	@Convert(converter = AnalogInConverter.class)
	private AnalogInType type;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;

	/**
	 * The analog in data enum.
	 * <p>
	 * This enum contains the names of the analog in data.
	 * 
	 * @author kaja
	 * @version 1.0
	 */
	
	public enum AnalogInType {

		ART("Art"), PRE("Pre"), ART_MON("Art Mon"), VEN("Ven"), DIA_IN("Dia In"), DIA_OUT("Dia Out"), ART_BLOOD(
				"Art Blood"), BLD("Bld"), VEN_CLAMP("Ven Clamp"), VEN_BLOOD("Ven Blood");
		
		private final String type;

		AnalogInType(String type) {
			this.type = type;
		}

		public String getType() {
			return type;
		}

		public static AnalogInType fromType(String type) {
			for (AnalogInType analogInType : AnalogInType.values()) {
				if (analogInType.getType().equals(type)) {
					return analogInType;
				}
			}
			throw new UnsupportedOperationException("The type " + type + " is not supported!");
		}

	}

}
