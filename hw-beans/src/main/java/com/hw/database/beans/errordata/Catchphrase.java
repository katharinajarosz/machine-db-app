package com.hw.database.beans.errordata;

import java.util.HashSet;
import java.util.Set;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "Catchphrase", schema = "error")
@ManagedBean
public class Catchphrase {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="CatchphraseId")
	private int id;
	
	private String translation;
	
	private String description;
	
	@OneToMany(mappedBy = "catchphrase")
	private Set<ErrorCatchphrase> errors = new HashSet<ErrorCatchphrase>();

}
