package com.hw.database.beans.controlsystem.types;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The digital input type data base bean. 
 * <p>
 * Contain process image id and identifier.
 * 
 * @author kaja
 * @version 1.0
 *
 */

@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@ManagedBean
public class DigitalInputType {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Digital_Input_Type_ID")
	private int id;
	
	@Column(name = "Identifier")
	private String identifier;

}
