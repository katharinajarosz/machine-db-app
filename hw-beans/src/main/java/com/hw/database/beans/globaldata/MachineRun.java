package com.hw.database.beans.globaldata;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.hw.database.beans.controlsystem.IOData;
import com.hw.database.beans.errordata.ActiveError;
import com.hw.database.beans.processcontrol.ProtectState;
import com.hw.database.beans.processcontrol.RunningSystemState;
import com.hw.database.beans.protectivesystem.AnalogIn;
import com.hw.database.beans.protectivesystem.PSConfiguration;
import com.hw.database.beans.protectivesystem.PSMissedMessage;
import com.hw.database.beans.protectivesystem.PSStatus;
import com.hw.database.beans.protectivesystem.PumpData;
import com.hw.database.beans.protectivesystem.SondData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The machine run data base bean:
 * <p>
 * Contain unidirectional relationships to {@link MachineInfo}; {@link SondDataConfiguration}, 
 * {@link Clinics}, {@link CalibrationData}, {@link SWVersions} 
 * <br/>and bidirectional 
 * relationships to {@link ActiveError}, {@link MachineDataSet}, {@link PSConfiguration}, 
 * {@link RunningSystemState}, {@link ProtectState}, {@link PSStatus}, 
 * {@link PSMissedMessage} and {@link IOData}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "MachineRun", schema = "machine")
@ManagedBean
public class MachineRun {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Machine_Run_ID")
	private long ID;

	@Column(name = "File_Name")
	private String fileName;

	@Column(name = "Start")
	private Timestamp start;

	@Column(name = "Ende")
	private Timestamp end;

//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "Machine_Info_ID")
//	private MachineInfo machineInfo;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "Sond_Data_Config_ID")
	private SondDataConfiguration sondDataConfig;

//	@ManyToOne(cascade = CascadeType.ALL)
//	@JoinColumn(name = "Clinic_ID")
//	private Clinics clinic;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "MachineRun_CalibratonData", joinColumns = {
			@JoinColumn(name = "Machine_Run_ID") }, inverseJoinColumns = {
					@JoinColumn(name = "Calibration_Data_ID") })
	private List<CalibrationData> calibrationData;

//	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//	@JoinTable(name = "MachineRun_SWVersions", joinColumns = {
//			@JoinColumn(name = "Machine_Run_ID") }, inverseJoinColumns = { 
//					@JoinColumn(name = "SW_Version_ID") })
//	private List<SWVersions> swVersions;
	
	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<ActiveError> activeErrors;	
	
//	@OneToOne(mappedBy = "machineRun", cascade = CascadeType.ALL)
//	private PSConfiguration psConfig;	

	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<RunningSystemState> systemStates;	

	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<ProtectState> protectStates;	

	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<PSStatus> psStatus;	

	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<PSMissedMessage> psMissedMessage;

	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<IOData> ioData;
	
	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<AnalogIn> analogIn;
	
	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<PumpData> sondData;
	
	@OneToMany(mappedBy = "machineRun", cascade = CascadeType.ALL)
	private List<SondData> pumpData;

}
