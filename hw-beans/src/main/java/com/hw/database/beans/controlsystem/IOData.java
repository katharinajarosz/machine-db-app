package com.hw.database.beans.controlsystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.controlsystem.types.GpModeType;
import com.hw.database.beans.controlsystem.types.HeaterModeType;
import com.hw.database.beans.controlsystem.types.PpSupplyModeType;
import com.hw.database.beans.controlsystem.types.PpWasteModeType;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RIOData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The input/output data base bean.
 * <p>
 * Contain a bidirectional relationship to {@link MachineRun}
 * and unidirectional relationships to {@link PpSupplyModeType}, 
 * {@link PpWasteModeType}, {@link GpModeType} and {@link HeaterModeType}.
 * 
 * @author kaja
 * @version 1.0
 * @see RIOData
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "IOData", schema = "machine")
@ManagedBean
public class IOData {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IO_Data_ID")
	private long ioDataId;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "PP_Supply_Mode_Type_ID")
	private PpSupplyModeType supplyMode;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "PP_Waste_Mode_Type_ID")
	private PpWasteModeType wasteMode;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "GP_Mode_Type_ID")
	private GpModeType gpMode;
	
	@ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
	@JoinColumn(name = "Heater_Mode_Type_ID")
	private HeaterModeType heaterMode;
	
	@Column(name = "Start")
	private Timestamp start;
	
	@Column(name = "Ende")
	private Timestamp end;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;

}
