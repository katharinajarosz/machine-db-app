package com.hw.database.beans.processcontrol;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The protect state data base bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineRun}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "ProtectState", schema = "machine")
@ManagedBean
public class ProtectState {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
    @Column(name = "Protect_State_ID",unique=true, nullable = false)
	private Long id;
	
	@Column(name="Protect_State")
	private int protectState;
	
	@Column(name="Protect_Substate")
	private int protectSubstate;
	
	@Column(name = "Start")
	private Timestamp start;
	
	@Column(name = "Ende")
	private Timestamp end;
	
	@ManyToOne(optional = false, fetch = FetchType.LAZY)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
}
