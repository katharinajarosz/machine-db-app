package com.hw.database.beans.errordata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An error level data base bean.
 * <p>
 * Contain process image id and identifier.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "ErrorSeverity", schema = "error")
@ManagedBean
public class ErrorLevel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Error_Level_ID")
	private int errorLevelId;
	
	@Column(name="Identifier")
	private String identifier;
	
	@Column(name="Severity")
	private String severity;

}
