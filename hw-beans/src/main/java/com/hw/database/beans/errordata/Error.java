package com.hw.database.beans.errordata;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An error data base bean.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "Error", schema = "error")
@ManagedBean
public class Error {
	
	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Polarion_ID")
	private int errorId;
	
	@Column(name="Error_Code")
	private String code;
	
	@Column(name="State")
	private String state;
	
	@Column(name="Condition")
	private String condition;
	
	@Column(name="Escalation")
	private String escalation;
	
	@Column(name="Deprecated")
	private String deprecated;
	
	@Column(name="Abortion")
	private String abortion;
	
	@Column(name="Acknowledgeable")
	private String acknowledgeable;
	
	@Column(name="Reviewer")
	private String reviewer;
	
	@Column(name="Reviewed")
	private String reviewed;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "Error_Level_ID")
	private ErrorLevel errorLevel;
	
	@OneToMany(mappedBy = "error", 
			cascade = CascadeType.ALL,
	        orphanRemoval = true)
	private List<ErrorCatchphrase> catchphrases;
	
	@Column(name="Error_Text_User")
	private String errorTextUser;
	
	@Column(name="Solution_for_User")
	private String solutionForUser;
	
	@Column(name="Error_Text_Service")
	private String errorTextService;
	
	@Column(name="Solution_for_Service")
	private String solutionForService;
	
	
	

}
