package com.hw.database.beans.protectivesystem;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The protect system messages data base bean.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
//@Entity
@ManagedBean
public class PSMessage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "PS_Messages_ID")
	private long psMessagesId;
	
	@Column(name ="Message_ID")
	private int messageId;
	
	@Column(name ="Message_Type")
	private String type;
	
	@Column(name ="Message_Direction")
	private String direction;

}
