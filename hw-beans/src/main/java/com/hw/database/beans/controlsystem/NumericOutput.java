package com.hw.database.beans.controlsystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.controlsystem.types.NumericOutputType;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RNumericOutput;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The numeric output data base bean.
 * <p>
 * Contain a bidirectional relationship to {@link MachineDataSet} 
 * and an unidirectional relationship to {@link NumericOutputType}.
 * 
 * @author kaja
 * @version 1.0
 * @see RNumericOutput
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "NumericOutput", schema = "machine")
@ManagedBean
public class NumericOutput {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "NumericOutputId")
	private long id;
	
	@Column(name = "DEM")
	private float dem;
	
	@Column(name = "CD")
	private float cd;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@Column(name = "Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@ManyToOne
	@JoinColumn(name = "Numeric_Output_Type_ID")
	private NumericOutputType type;
	

}
