package com.hw.database.beans.processcontrol;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "SystemState", schema = "machine")
@ManagedBean
public class SystemState {
	
	@Id
	@Column(name = "System_State_Node", columnDefinition = "HIERARCHYID")
	private Byte[] systemStateNode;
	
	@Column(name="System_State_Level", columnDefinition = "AS System_State_Node.GetLevel()")
	private short systemStateLevel;
	
	@Column(name = "System_State_Value")
	private int systemStateValue;
	
	@Column(name = "System_State_Name")
	private String systemStateName;
	
	@Column(name = "Node_String", columnDefinition = "AS System_State_Node.ToString()")
	private String nodeString;
		
}
