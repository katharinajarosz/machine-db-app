package com.hw.database.beans.controlsystem;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.controlsystem.types.ClosedLoopType;
import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@SuppressWarnings("serial")
//@Embeddable
//@Data
//@Builder @AllArgsConstructor
//@ManagedBean
public class ClosedLoopIdentity implements Serializable {

//	@NotNull
//	@ManyToOne
//	@JoinColumn(name = "Machine_Run_ID")
//	private MachineRun machineRunId;
//	@NotNull
//	private Timestamp machineDataLoopTimestamp;
//	@NotNull
//	@ManyToOne
//	@JoinColumn(name = "Closed_Loop_Type_ID")
//	private ClosedLoopType type;
//	
}