package com.hw.database.beans.globaldata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An address data base bean.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
// @Entity
// @ManagedBean
public class Address {
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "ID_Address_Data")
	private long ID;
	
	private String street;
	
	private String number;
	
	private int plz;
	
	private String city;

}
