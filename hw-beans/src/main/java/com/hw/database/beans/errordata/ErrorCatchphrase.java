package com.hw.database.beans.errordata;

import java.io.Serializable;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "ErrorCatchphrase", schema = "error")
@ManagedBean
public class ErrorCatchphrase {
	
	@EmbeddedId
	private ErrorCatchphraseId id; 
	
	@ManyToOne
	@JoinColumn(name = "fk_error", insertable = false, updatable = false)
	private Error error;
	
	@ManyToOne
	@JoinColumn(name = "fk_catchphrase", insertable = false, updatable = false)
	private Catchphrase catchphrase;
	
	@Column(name = "weight")
	private double weight;
	
	@Column(name = "comment")
	private String comment;
	
	@Embeddable
	@Builder @NoArgsConstructor @AllArgsConstructor
	public static class ErrorCatchphraseId implements Serializable {
				
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Column(name = "fk_error")
		private long errorId;
		
		@Column(name = "fk_catchphrase")
		private long catchphraseId;
		
	}

}
