/**
 * 
 */
package com.hw.database.beans.machinedataset;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RSystemDiagnosis;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The system diagnosis data base bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineDataSet}.
 * 
 * @author kaja
 * @version 1.0
 * @see RSystemDiagnosis
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "SystemDiagnosis", schema = "machine")
@ManagedBean
public class SystemDiagnosis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SystemDiagnosisId")
	private long id;

	@Column(name = "Processor_Load")
	private float processorLoad;
	
	@Column(name = "Memory_Load")
	private float memoryLoad;
	
	@Column(name = "System_Loop_Processing_Time")
	private float systemLoopProcessingTime;
	
	@Column(name = "Loop_Late_Counter")
	private float loopLateCounter;
	
	@Column(name = "Host_Processor_Load")
	private float hostTotalProcessorLoad;
	
	@Column(name = "Host_LK_Processor_Load")
	private float hostLK2001ProcessorLoad;
	
	@Column(name = "Host_LK_Bytes")
	private float hostLK2001PrivateBytes;
	
	@Column(name = "Host_Reconnect_Counter")
	private float hostReconnectCounter;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@Column(name = "Timestamp")
	private Timestamp machineDataLoopTimestamp;
}
