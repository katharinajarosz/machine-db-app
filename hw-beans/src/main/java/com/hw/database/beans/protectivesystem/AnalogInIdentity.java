package com.hw.database.beans.protectivesystem;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.protectivesystem.AnalogIn.AnalogInType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@SuppressWarnings("serial")
//@Embeddable
//@Data
//@Builder @AllArgsConstructor
public class AnalogInIdentity implements Serializable {

//	@NotNull
//	@ManyToOne
//	@JoinColumn(name = "Machine_Run_ID")
//	private MachineRun machineRun;
//	@NotNull
//	private Timestamp machineDataLoopTimestamp;
//	@NotNull
//	@Column(name = "Type")
//	@Convert(converter = AnalogInConverter.class)
//	private AnalogInType type;

//	public AnalogInIdentity(MachineRun machineRun, LocalDateTime timestamp, AnalogInType type) {
//		this.machineRun = machineRun;
//		this.machineDataLoopTimestamp = timestamp;
//		this.type = type;
//	}
	
}