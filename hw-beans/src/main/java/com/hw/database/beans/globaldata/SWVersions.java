package com.hw.database.beans.globaldata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The software versions data base bean.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
// @Entity
// @ManagedBean
public class SWVersions {
	
//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	@Column(name = "SW_Version_ID")
	private long ID;
	
	// @Column(name = "SW_Name")
	private String swName;
	
	// @Column(name = "SW_Version_Number")
	private int swVersionNr;

}
