package com.hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;


/**
 * The Class Application.
 * 
 * Starts the application.
 */
@SpringBootApplication
public class Application {
	
	// private static final Logger LOGGER=LoggerFactory.getLogger(Application.class);


	   
    	/**
	     * The main method.
	     *
	     * @param args the arguments
	     * @throws Exception the exception
	     */
	    public static void main(String[] args) throws Exception {
	        SpringApplication.run(Application.class, args); 
	       // log.info("Application started");
	        // log.info("Simple log statement with inputs {}, {} and {}", 1,2,3);
	       
	    }

}